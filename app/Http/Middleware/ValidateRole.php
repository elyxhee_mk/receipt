<?php

namespace receipt\Http\Middleware;
use receipt\Data\Models\User;

use Closure;

class ValidateRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        //dd($request);
        $adminrole = array(1,5);
        $input = $request->only('id','user_id');        
        $isAdmin = User::where("users.id","=",$input['user_id'])->join('user_roles','users.id','=','user_roles.user_id')->wherein('user_roles.role_id',$adminrole)->first();

        //dd("isAdmin",$isAdmin);
        //dd( $isAdmin->toSql(), $isAdmin->getBindings());
        if($isAdmin == NULL)
        {
            $code = 401;
            $output = ['error' => [ 'code' => $code, 'messages' => [ 'You dont have permission to perform this action.' ] ] ];
            return response()->json($output, $code);
        }        
        return $next($request);
    }
}
