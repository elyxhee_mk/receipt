<?php

namespace receipt\Http\Middleware;

use Closure;

class VerifyAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('key')) {
            return $next($request);
        }else{
            return redirect('/');
        }
    }
}
