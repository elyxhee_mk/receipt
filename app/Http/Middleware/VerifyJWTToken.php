<?php

namespace receipt\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;


class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $token = JWTAuth::getToken();             
            $user = JWTAuth::toUser($token);
            $request['user_id'] = $user['id'];
            
        }catch (JWTException $e) {
            if($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json(['token_expired'], $e->getStatusCode());
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json(['token_invalid'], $e->getStatusCode());
            }else{
                return response()->json(['error'=>'Token is required']);
            }
        }
       return $next($request);
    }
    public function handle1($request, Closure $next)
    {
        
       //dd("validatetoken");
        $token = JWTAuth::getToken();
        if(!$token)
        {
            $code = 400;
            $output = ['error' => ['code' => $code, 'messages' => ['Token Required.']]];                
            return response()->json($output, $code);
        }

        else if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $request['user_id'] = $claims->get('sub');        
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                return response()->json($output, $code);
                
            }
        }
           
        return $next($request);
    }
}