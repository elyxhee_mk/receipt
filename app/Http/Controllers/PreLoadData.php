<?php

namespace receipt\Http\Controllers;

use Illuminate\Http\Request;

class PreLoadData extends Controller
{
    //
    private $gender;
    private $recial;
    private $language;
    private $interest;
    private $performance;
    private $user_experience;
    private $school_grade;
    private $hear_about_us;
    private $are_you;
    private $are_you_participat;
    protected $data;

    public function __construct(){
    	$this->data = array();
        $this->gender = array("Male","Female");
        $this->recial = array("Medical");
        $this->language = array("US English");
        $this->interest = array("Sports & Reading");
        $this->performance = array("Art School");
        $this->user_experience = array("Yes","No");
        $this->school_grade = array("10 Grade","11 Grade");
        $this->hear_about_us = array("Social Media","Friend");
        $this->are_you = array("Musician","Artist");
        $this->are_you_participat = array("Yes","No");


    }
   
    public function get(){

       /* print("test");
        die();
        //echo '<pre>';
            //print_r(getallheaders());
        dd("get");*/

		$data["gender"] = $this->gender;
		$data["recial"] = $this->recial;
		$data["language"] = $this->language;
		$data["interest"] = $this->interest;
		$data["performance"] = $this->performance;
		$data["user_experience"] = $this->user_experience;
		$data["school_grade"] = $this->school_grade;
		$data["hear_about_us"] = $this->hear_about_us;
		$data["are_you"] = $this->are_you;        
        $data["are_you_participat"] = $this->are_you_participat;

        return response()->json(['code'=>200,'message'=>'Data Supplied Successfully','data'=>$data]);
    }
}
