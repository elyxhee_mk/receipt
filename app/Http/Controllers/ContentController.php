<?php

namespace receipt\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Image,Storage;
use receipt\Data\Models\TextContent;
use Validator;

class ContentController extends Controller
{
   

    public function __construct() {        
               
    }
    public function add(Request $request) {

    	
    	$input = $request->only('user_id','content_type','content_text');
        //dd($request->all());
             
        $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',
            'content_text'        =>  'required',            
            'content_type'         =>  'required|in:TC,PP,US'            
        ];       
        
        $validator = Validator::make($input, $rules);        
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                //dd($validator->messages()->messages());
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
        	unset($input["user_id"]);          
            $exists = TextContent::where("content_type",$input['content_type'])->first();
            //dd()
            if($exists == NULL)
            {

                $content = new TextContent();
                $content->content_type = $input['content_type'];
                $content->content_text = $input['content_text']; 
                if($content->save()){
                    $code = 200;
                        $output = ['error'=>['code'=>$code,'messages'=>['Content Added Successfully.']]];
                } 
                else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating content.']]];
                }
            }
            else{
                        $code = 406;
                        $output = ['error'=>['code'=>$code,'messages'=>['Content Already Added']]];
                }
        }

        return response()->json($output, $code);
    }
    
    public function all(Request $request)
    {
    	$input = $request->only('user_id','content_type');
	//dd($input);
        
         $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',
            'content_type'         =>  'required|in:TC,PP,US'            
            ];
    	
        $validator = Validator::make($input, $rules);
       
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
        	      
            $content = TextContent::where("content_type",$input['content_type'])->first();
            //dd("response id",$content->content_text);
            $response = $content["content_text"];
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
                            'data' => $content,
				'messages'=> ['content supplied successfully'],
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while getting content.']]];
                    }
        }

        return response()->json($output, $code);
    }
  
    public function delete(Request $request)
    {
      $input = $request->only('user_id','content_type');
	//dd($input);
        
         $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',
            'content_type'         =>  'required|in:TC,PP'            
            ];
    $validator = Validator::make($input, $rules);
    
    if ($validator->fails()) {
        if(array_key_exists("user_id", $validator->messages()->messages())){
            //dd($validator->messages()->messages());
            $code = 401;
            $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
        }else{
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        }
    } else {
        $response = TextContent::where("content_type",$input['content_type'])->delete();
        if ($response) {
                $code = 200;
                $output = ['error'=>['code'=>$code,'messages'=>['content deleted Successfully']]];
                }else{
                    $code = 409;
                    $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating course.']]];
                }
        }

        return response()->json($output, $code); 
    }
        
    
}
