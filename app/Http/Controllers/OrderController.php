<?php



namespace receipt\Http\Controllers;



use Illuminate\Http\Request;

use receipt\Http\Controllers\Controller;

use receipt\Data\Models\Order;

use receipt\Data\Models\User;

use receipt\Data\Repositories\OrderRepository;

use Validator, \App;



class OrderController extends Controller

{

    //

    public function __construct(OrderRepository $repo,User $user)

    {

    	$this->repository = $repo;

    	$this->per_page = 50;

        $this->user = $user;

    }

    public function add(Request $request)

    {

    	//dd("add order");

    	$input = $request->only('user_id','package','order_amount','payment_mode');    	

    	 	

    	$rules = [

            'user_id'             =>  'required|exists:users,id,visibility,1',

            'payment_mode'			=>     'required|in:CC,CASH', 

            'package'			=>'required|in:Economy,Deluxe'       

            

        ];

         $messages = [

                    'cart_product.required' => 'No Product is Selected'

        ];        

        $validator = Validator::make($input,$rules);



        if($validator->fails()) {



            if(array_key_exists("user_id", $validator->messages()->messages())){

                $code = 401;

                $output = ['error' => [ 'code' => $code, 'messages' => 'You are not able to perform this action you may be removed or disabled by admin' ] ];

            }else{

                $code = 406;

                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];

            }

        }

        else

        {

	      

	        

	        

	        $data = array();

	        $data["customer_id"] = $input['user_id'];

	        $data["amount"] = $input['order_amount'];	        

	        $data["payment_mode"] = $input['payment_mode'];

	        $data["package"] =$input['package'];

            //dd($data);

	        $order = $this->repository->create($data);

	        if($order != NULL )

	        {

	         	$code = 200;

	            $output['response']['code'] = $code;
		    $output['response']['messages'] = ['Order Created Successfully'];

	            $output['response']['data'] = $order;

	        }

	        else {

	                $code = 400;

	                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Request.']]];

	            } 

	     } 

        return response()->json($output);     

    	

    }

    public function all(Request $request,Order $order){



    	$input = $request->only('user_id','customer_id','order_date','paid','paid_date','order_id');

    	$input = array_filter($input);

    	if(isset($input['order_id']) && $input['order_id'] != "")

    		$input['order_id'] = hashid_decode($input['order_id']);

    	

    	$rules = [

            'user_id'             =>  'required|exists:users,id,visibility,1',

            'customer_id'     =>  'sometimes|required|exists:users,id,visibility,1', 

            'order_date'			=>'sometimes|date_format:Y-m-d', 

            'paid_date'			=>'sometimes|date_format:"Y-m-d',

            'paid'			=>'sometimes|in:true,false',

            'order_id'     =>  'sometimes|required|exists:orders,id,deleted_at,NULL',            

            

        ];

               

        $validator = Validator::make($input,$rules);



        if($validator->fails()) {



            if(array_key_exists("user_id", $validator->messages()->messages())){

                $code = 401;

                $output = ['error' => [ 'code' => $code, 'messages' => 'You are not able to perform this action you may be removed or disabled by admin' ] ];

            }else{

                $code = 406;

                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];

            }

        }

        else

        {

	      

	        if(isset($input['order_id']) && $input['order_id'] != "")

	        {

	        	$orders = $this->repository->findById($input['order_id']);

	        }

	        else

	        	$orders = $this->repository->findByAll(true,$this->per_page,$input);



	        if($orders != NULL )

	        {

	         	$code = 200;

	            $output['response']['code'] = $code;
			$output['response']['messages'] = ['Order list provided Successfully'];

	            $output['response']['data'] = $orders;

	        }

	        else {

	                $code = 400;

	                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Request.']]];

	            } 

	     } 

        return response()->json($output);     

    }

    public function update(Request $request)

    {



    	//dd("in update");

    	$input = $request->only('user_id','amount','order_id','invoice_no','shipping_method','payment_mode','pay');

    	$input['id'] = hashid_decode($input['order_id']);

    	$input = array_filter($input);

    	

    	$rules = [

            'user_id'             =>  'required|exists:users,id,visibility,1',

            'id'     =>  'required|exists:orders,id,deleted_at,NULL',            

            'amount'			=>     'sometimes|numeric', 

            'invoice_no'			=>'sometimes|exists:order_invoices,id,deleted_at,NULL', 

            'shipping_method'     =>  'sometimes|in:standard,custom',           

            'payment_mode'			=>     'sometimes|in:cash,cc',

            'pay'			=>     'sometimes|in:true,false'                                                                      

            

        ];

               

        $validator = Validator::make($input,$rules);



        if($validator->fails()) {



            if(array_key_exists("user_id", $validator->messages()->messages())){

                $code = 401;

                $output = ['error' => [ 'code' => $code, 'messages' => 'You are not able to perform this action you may be removed or disabled by admin' ] ];

            }else{

                $code = 406;

                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];

            }

        }

        else

        {

	       	

	        unset($input["user_id"]);

	        unset($input["order_id"]);

	        if(isset($input['pay']))

	        {

	        	$input['paid'] = ($input['pay'] == "true") ? 1:0;

	        	$input['paid_date'] = date('Y-m-d h:i:s');

	        	unset($input['pay']);

	    	}

	    	$orders = $this->repository->update($input);



            if($orders->paid == 1)// upgrade package

            {

               

                $this->user->where("id",hashid_decode($orders->customer_id))->update(array("subscription"=>$orders->package));

            }

	        

	        if($orders != NULL )

	        {

	         $code = 200;	            
		$output['response']['messages'] = ['Order Updated Successfully'];
		$output['response']['code'] = $code;
	        $output['response']['data'] = $orders;

	        }

	        else {

	                $code = 400;

	                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Request.']]];

	            } 

	     } 

        return response()->json($output);     

    }

}

