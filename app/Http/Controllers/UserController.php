<?php

namespace receipt\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Socialite;
use receipt\Http\Controllers\Controller;
use Illuminate\Support\Facades\Event;
use receipt\Events\SendUserActivation;
use receipt\Events\PasswordWasRecovered;
use receipt\Events\PasswordWasReset;
use receipt\Data\Models\UserSocialAccount;
use receipt\Data\Repositories\UserRepository;
/*use receipt\Data\Models\UserFriend;
use receipt\Data\Models\UserProfileAction;*/
use receipt\Data\Models\User;
use receipt\Data\Models\Feedback;
use Tymon\JWTAuth\Payload;
use Validator, JWTAuth;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Image,Storage;

class UserController extends Controller {
    const PER_PAGE = 10;
    private $_repository;

    public function __construct(UserRepository $user, UserSocialAccount $userSocialAccount) {
        
        $this->_repository = $user;
        $this->model_user_social_account = $userSocialAccount;
    }
	public function register(Request $request) {

        $input = $request->only('email', 'password','user_type','file','device_token','udid','device_type','social_network_id','signup_via','username','first_name','last_name','confirm_password');
      
        $input = array_filter($input,'strlen');
        if($input['signup_via'] == "email")
	{
            $rules['file'] = 'sometimes|mimes:jpeg,jpeg,png';

        $rules['username'] = 'sometimes|max:30';
        $rules['password'] =  'required|min:6';        
        $rules['confirm_password']     = 'required | same:password|min:6';
        $rules['last_name'] =  'required|max:50';
        $rules['first_name'] =  'required|max:50';
	}
        $rules['signup_via'] =  'required|in:email,facebook,twitter,google'; 
        if($input['signup_via'] == "facebook" || $input['signup_via'] == "twitter" || $input['signup_via'] == "google") 
            $rules['social_network_id'] =  'required';
	else
		$rules['email'] =  'required|email|unique:users,email';        

        $messages = [
                    //'file.required' => 'Please upload profile picture.',
                    'file.mimes' => 'Image should be in PNG or JPEG format.'
        ];               
        
        $validator = Validator::make($input, $rules,$messages);       
        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
            unset($input['confirm_password']);
            if(isset($input['device_token']) && isset($input['udid']) && isset($input['device_type'])){
                $deviceData['token'] = $input['device_token'];
                $deviceData['udid'] = $input['udid'];
                $deviceData['type'] = $input['device_type'];
                unset($input['device_token'],$input['udid'],$input['device_type']);        
            }
            if( $input['signup_via'] == "facebook" || $input['signup_via'] == "google" ||$input['signup_via'] == "twitter"){
                try {
                        $userExist = $this->_repository->model_user_social_account
                                        ->where('social_network_id', '=', $input['social_network_id'])
                                        ->where('social_network','=',$input['signup_via'])->first();
                        
                        if($userExist == NULL){
                            // email does not exist
                            $registerUser = $this->_repository->register($input);
                            if($registerUser){
                                Event::fire(new SendUserActivation($registerUser));
                            }
                            $message = "You have registered successfully.";
                        } else {
                            // email exist
                            $id = $userExist->user_id;
                           /* $deviceData = array('user_id' => $id,'token' => $input['device_token'],'udid' => $input['udid'],'type' => $input['device_type'] );
                            $isDeviceData = $this->deviceRepo->setUserTokens($deviceData);*/
                            $user = $this->_repository->findById($id,true,true,false,false);
                                $registerUser = $this->_repository->update([
                                    'id' => $id,
                                    'login_at' => Carbon::now(),
                                    'access_token' => JWTAuth::fromUser($user),
                                ],true,true);
                            $message = "You are already registered.";
                        }

                        if ($registerUser) {
                            $code = 200;
                            $output = [ 'response' => [ 'code' => $code, 'messages' => [ $message ] ] ];
                            $output['response']['data'] = $registerUser;
                            
                        } else {
                            $code = 401;
                            $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while registration, please try later' ] ] ];
                        }

                } catch(\GuzzleHttp\Exception\ClientException $e){
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'Looks like your facebook access token has expired or is invalid.' ] ] ];
                } catch(\GuzzleHttp\Exception\ServerException $e){
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'Looks like your facebook access token has expired or is invalid.' ] ] ];
                } catch(Exception $e){
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'Looks like your facebook access token has expired or is invalid.' ] ] ];
                } 
            } else {
            $userAlredyRegistered = $this->_repository->model->where('users.email', '=', $input['email'])->first();
            if($userAlredyRegistered == NULL){
                if(isset($input['file']) && isset($input['file']) != "")
                {
                    $file_name = $input['file']->store(config('app.files.users.folder_name'));
                    $input['profile_pic'] = $input['file']->hashName();
                }
                $registerUser = $this->_repository->register($input);
                $message = "You have registered successfully.";

                if ($registerUser) {
                    // Event::fire(new SendUserActivation($registerUser));
                    $code = 200;
                    $output = [ 'response' => [ 'code' => $code, 'messages' => [ $message ] ] ];
                    $output['response']['data'] = $registerUser;
                    
                } else {
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while registration, please try later' ] ] ];
                }
            } else {
                $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'User already registered with this email address.' ] ] ];
            }
        }
        }

        return response()->json($output, $code);
    }
   
    //---------------------
    public function register_facebook(Request $request) {

        $input = $request->only('email', 'password', 'first_name','last_name','business_name','username', 'age','zip_code', 'dob','user_type','gender','business_established_at','sponser','package','business_location','file','device_token','udid','device_type','social_network_id','signup_via');
        // dd($input);
        $input = array_filter($input,'strlen');
        if(isset($input['business_location'])){
            $input['business_location'] = is_array($input['business_location'])?$input['business_location']:json_decode($input['business_location']);
        }
        
        // dd($input);

        $rules = [
            'user_type'    =>  'required|in:business,simple',
            'signup_via'    =>  'required|in:email,facebook',
            'file' => 'required|mimes:jpeg,jpeg,png'
        ];
        $messages = [
                    'file.required' => 'Please upload profile picture.',
                    'file.mimes' => 'Image should be in PNG or JPEG format.'
        ];
                $rules['first_name'] = 'required';
                $rules['password'] =  'required|min:6';
                $rules['email'] =  'required|email|unique:users,email';
                $rules['last_name'] =  'required';
                if(isset($input['signup_via']) && $input['signup_via'] == 'facebook'){
                    $rules['social_network_id'] = 'required';
                }
                if($input['user_type'] == 'business'){
                    $rules['business_name'] = 'required';
                    $rules['package'] =  'required';
                    $rules['sponser'] =  'required';
                    $rules['business_established_at'] =  'required';
                    // $rules['business_location'] =  'required|array';
                    unset($input['username'],$input['age'],$input['zip_code'],$input['dob'],$input['gender']);
                    
                }else{
                    $rules['username'] = 'required';
                    $rules['age'] =  'required|int';
                    $rules['zip_code'] =  'required|int';
                    $rules['dob'] =  'required';
                    $rules['gender'] =  'required|in:male,female';
                    unset($input['business_name'],$input['package'],$input['sponser'],$input['business_established_at'],$input['business_location']);

                }
        
        $validator = Validator::make($input, $rules,$messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
            if(isset($input['device_token']) && isset($input['udid']) && isset($input['device_type'])){
                $deviceData['token'] = $input['device_token'];
                $deviceData['udid'] = $input['udid'];
                $deviceData['type'] = $input['device_type'];
                unset($input['device_token'],$input['udid'],$input['device_type']);        
            }
            
            if( $input['signup_via'] == "facebook"){
                try {
                        $userExist = $this->_repository->model_user_social_account
                                        ->where('social_network_id', '=', $input['social_network_id'])
                                        ->where('social_network','=','facebook')->first();
                        
                        if($userExist == NULL){
                            // email does not exist
                            $registerUser = $this->_repository->register($input);
                            /*if($registerUser){
                                Event::fire(new SendUserActivation($registerUser));
                            }*/
                            $message = "You have registered successfully.";
                        } else {
                            // email exist
                            $id = $userExist->user_id;
                            $deviceData = array('user_id' => $id,'token' => $input['device_token'],'udid' => $input['udid'],'type' => $input['device_type'] );
                            $isDeviceData = $this->deviceRepo->setUserTokens($deviceData);
                            $user = $this->_repository->findById($id,true,true,false,false);
                                $registerUser = $this->_repository->update([
                                    'id' => $id,
                                    'login_at' => Carbon::now(),
                                    'access_token' => JWTAuth::fromUser($user),
                                ],true,true);
                            $message = "You are already registered.";
                        }

                        if ($registerUser) {
                            $code = 200;
                            $output = [ 'response' => [ 'code' => $code, 'messages' => [ $message ] ] ];
                            $output['response']['data'] = $registerUser;
                            
                        } else {
                            $code = 401;
                            $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while registration, please try later' ] ] ];
                        }

                } catch(\GuzzleHttp\Exception\ClientException $e){
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'Looks like your facebook access token has expired or is invalid.' ] ] ];
                } catch(\GuzzleHttp\Exception\ServerException $e){
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'Looks like your facebook access token has expired or is invalid.' ] ] ];
                } catch(Exception $e){
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'Looks like your facebook access token has expired or is invalid.' ] ] ];
                } 
            } else {
                $userAlredyRegistered = $this->_repository->model->where('users.email', '=', $input['email'])->first();
                if($userAlredyRegistered == NULL){
                    $file_name = $input['file']->store(config('app.files.users.folder_name'));
                    $input['profile_pic'] = $input['file']->hashName();
                    // dd($input);
                    // signup via email
                    $registerUser = $this->_repository->register($input);
                    $message = "You have registered successfully.";

                    if ($registerUser) {
                        // Event::fire(new SendUserActivation($registerUser));
                        $code = 200;
                        $output = [ 'response' => [ 'code' => $code, 'messages' => [ $message ] ] ];
                        $output['response']['data'] = $registerUser;
                        
                    } else {
                        $code = 401;
                        $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while registration, please try later' ] ] ];
                    }
                } else {
                    $code = 401;
                        $output = ['error' => [ 'code' => $code, 'messages' => [ 'User already registered with this email address.' ] ] ];
                }
            }

        }

        return response()->json($output, $code);
    }
    public function register_bkup(Request $request) {

        $input = $request->only('email', 'password','user_type','file','device_token','udid','device_type','social_network_id','signup_via','username','first_name','last_name','confirm_password');
      
        $input = array_filter($input,'strlen');

        $rules['file'] = 'sometimes|mimes:jpeg,jpeg,png';
        $rules['username'] = 'sometimes|max:30';
        $rules['password'] =  'required|min:6';
        $rules['email'] =  'required|email|unique:users,email';
        //$rules['last_name'] =  'required';
        $rules['signup_via'] =  'required|in:email,facebook,twitter,google';
	$rules['confirm_password']     = 'required | same:password|min:6';
        $rules['last_name'] =  'required|max:50';
        $rules['first_name'] =  'required|max:50';        

        $messages = [
                    //'file.required' => 'Please upload profile picture.',
                    'file.mimes' => 'Image should be in PNG or JPEG format.'
        ];               
        
        $validator = Validator::make($input, $rules,$messages);       
        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
		unset($input['confirm_password']);
            if(isset($input['device_token']) && isset($input['udid']) && isset($input['device_type'])){
                $deviceData['token'] = $input['device_token'];
                $deviceData['udid'] = $input['udid'];
                $deviceData['type'] = $input['device_type'];
                unset($input['device_token'],$input['udid'],$input['device_type']);        
            }
            $userAlredyRegistered = $this->_repository->model->where('users.email', '=', $input['email'])->first();
            if($userAlredyRegistered == NULL){
                if(isset($input['file']) && isset($input['file']) != "")
                {
                    $file_name = $input['file']->store(config('app.files.users.folder_name'));
                    $input['profile_pic'] = $input['file']->hashName();
                }
                $registerUser = $this->_repository->register($input);
                $message = "You have registered successfully.";

                if ($registerUser) {
                    // Event::fire(new SendUserActivation($registerUser));
                    $code = 200;
                    $output = [ 'response' => [ 'code' => $code, 'messages' => [ $message ] ] ];
                    $output['response']['data'] = $registerUser;
                    
                } else {
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while registration, please try later' ] ] ];
                }
            } else {
                $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'User already registered with this email address.' ] ] ];
            }
        }

        return response()->json($output, $code);
    }

    public function update(Request $request) {

        $input = $request->only('password', 'first_name','last_name','user_type','file','old_password','subscription');
        ;
        $input = array_filter($input,'strlen');
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                if($claims instanceof Payload && $claims->get('sub')) {
                    $input['id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules = [
            'id'                        =>  'required|exists:users,id',
            'subscription'              =>  'sometimes|in:Free,Deluxe,Economy'
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            
            
        } else {
            
            if( isset($input['email']) ){
                $emailEmptyCheck = $this->_repository->model->where('id', '=', $input['id'])->first(['email']);

                if( $emailEmptyCheck->email != ""){
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'Email address already has been updated for this account.' ] ] ];
                    return response()->json($output, $code);
                }
            }
            if(isset($input['file'])){
                // $previousImage = $this->findById($input['id']);

                $file_name = $input['file']->store(config('app.files.users.folder_name'));
                    $input['profile_pic'] = $input['file']->hashName();
                    $saveImage = $this->crop($input['file']);
                    if($saveImage){
                        unset($input['file']);
                    }
            }
            //dd("input to update",$input);
            $updateUser = $this->_repository->update($input,true);
            $message = "Profile Updated successfully.";

            if ($updateUser) {
                // dd($updateUser);
                unset($updateUser->updated_at, $updateUser->deleted_at, $updateUser->role_id, $updateUser->access_token,$updateUser->recover_password_key,$updateUser->recover_attempt_at,$updateUser->create_password);
            
                $code = 200;
                $output = [ 'response' => [ 'code' => $code, 'messages' => [ $message ] ] ];
                $output['response']['data'] = $updateUser;
                
            } else {
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while registration, please try later' ] ] ];
            } 

        }

        return response()->json($output, $code);
    }

    public function referFriend(Request $request) {

        $input = $request->only('profile_id', 'share_id');
        $input = array_filter($input,'strlen');
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                return response()->json($output, $code);
            }
        }
        $rules = [
            'user_id'                        =>  'required|exists:users,id',
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            
            
        } else {
            
                // signup via email
                $updateUser = $this->_repository->referFriend($input,true);
                $message = "Profile has been refferd.";

            if ($updateUser) {
                $code = 200;
                $output = [ 'response' => [ 'code' => $code, 'messages' => [ $message ] ] ];
            } else {
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => [ 'An error occurred while registration, please try later' ] ] ];
            } 

        }

        return response()->json($output, $code);
    }
    

    // private function deleteImage($filePath, $extensions = ['jpeg', 'png', 'gif', 'bmp', 'svg']) {

    //     if (in_array($file->guessExtension(), $extensions)) {
    //         $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'@3x.'.$ext, $image->stream());
    //         $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'@2x.'.$ext, $image->resize($width / 1.5, $height / 1.5)->stream());
    //         $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'.'.$ext, $image->resize($width / 3, $height / 3)->stream());
    //     }
    //     return true;
    // }

    public function login(Request $request) {

        $input = $request->only('email', 'password','device_token','udid','device_type');   
        $rules['email'] = 'required|email|exists:users,email';
        $rules['password'] = 'required';
        

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
            
            // if( $input['login_via'] == "facebook" ){
                
            //     try {
            //         $user = Socialite::driver('facebook')
            //                 ->userFromToken($input['fb_access_token']);
            //                 $socialNetwork = NULL;
            //                 $socialNetwork = $this->model_user_social_account->where('social_network_id', '=', $user->id)->first(['user_id']);
            //                 if($socialNetwork != NULL){
                                
            //                     $user = $this->_repository->findById($socialNetwork->user_id,true,true,false,false);
            //                     if($user->visibility == 1){

            //                         $userExist = $this->_repository->update([
            //                             'id' => $socialNetwork->user_id,
            //                             'login_at' => Carbon::now(),
            //                             'access_token' => JWTAuth::fromUser($user),
            //                         ],true,true);
            //                         $code = 200;
            //                         $output = [
            //                         'response' => [
            //                             'code' => $code,
            //                             'message'=>['You have been logged in successfully'],
            //                             ]
            //                         ];
            //                         unset($userExist->password,  $userExist->recover_password_key,$userExist->recover_attempt_at, $userExist->updated_at, $userExist->deleted_at, $userExist->role_id,$userExist->created_at);
            //                         $output['response']['data'] = $userExist;
            //                     }else{
            //                         $code = 406;
            //                         $output = ['error' => [ 'code' => $code, 'messages' => [ 'Your account has been block.' ] ] ];
            //                     }
            //                 } else {
            //                     $code = 402;
            //                     $output = ['error' => [ 'code' => $code, 'messages' => [ 'You are not registered user.Please register' ] ] ];
            //                 }    
            //     } catch(\GuzzleHttp\Exception\ClientException $e){
            //         $code = 401;
            //         $output = ['error' => [ 'code' => $code, 'messages' => [ 'Looks like your facebook access token has expired or is invalid.' ] ] ];
            //     } catch(\GuzzleHttp\Exception\ServerException $e){
            //         $code = 401;
            //         $output = ['error' => [ 'code' => $code, 'messages' => [ 'Looks like your facebook access token has expired or is invalid.' ] ] ];
            //     } catch(Exception $e){
            //         $code = 401;
            //         $output = ['error' => [ 'code' => $code, 'messages' => [ 'Looks like your facebook access token has expired or is invalid.' ] ] ];
            //     } 

            // } else {
                $user = $this->_repository->login($input);
                if ($user) {
                    $code = 200;
                    $output = [
                        'response' => [
                        'code' => $code,
                        'message'=>['You have been logged in successfully'],
                        ]
                    ];
                    unset($user->password,  $user->recover_password_key,$user->recover_attempt_at, $user->updated_at, $user->deleted_at, $user->created_at);
                    $output['response']['data'] = $user;

                } else {
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'The email and password do not match' ] ] ];
                }
            // }
        }
        return response()->json($output, $code);
    }

    public function logout(Request $request) {

        try {
            $token = JWTAuth::getToken();
            JWTAuth::invalidate($token);
        } catch (\Exception $e) {}

        return response()->json([
            'response' => [
                'code' => 200,
                'messages' => [
                    'You have been successfully logged out.'
                ]
            ]
        ], 200);
    }

    public function all(Request $request) {
        
       
        //dd("all");
        $input = $request->only('pagination','keyword','filter_by_name','filter_by_type','filter_by_gender','lattitude','longitude','has_connected','device');
         $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules = ['user_id' => 'required|exists:users,id'];
        $messages = [];

        $validator = Validator::make($input, $rules, $messages);
        //dd($users);
        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];

        // if validation passes
        } else {
            $pagination = true;
            if($input['pagination']) {
                $pagination = $input['pagination'];
            }
            if(isset($input['device']) && ($input['device'] == 'android' || $input['device'] == 'ios')){
                    $input['filter_by_status'] = 1;    
                    $input['role_id'] = 2; 
            }
            //dd("calling All");
            $users = $this->_repository->findByAll($pagination, self::PER_PAGE, $input,false,true,true);
           
           // dd($users);
             $code = 200;
            $users['code'] = $code;
            $output['response'] = $users;
        }
          // dd("return data" ); 
        return response()->json($output);
    }
    public function view(Request $request) {
        
        $input = $request->only('id');
       // dd($request);
        $token = JWTAuth::getToken();
        //dd();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
        }
        if($input['id'] != NULL){
            $input['id'] = hashid_decode($input['id']);
        }else{
            $input['id'] = $input['user_id'];
        }
      // dd( $input['user_id']);
        $rules = [
                    'user_id' => 'required|exists:users,id',
                    'id' => 'required|exists:users,id'
                ];
        $messages = [
                    'user_id.exists' => 'You are not allow to Perform this action.',   
                    'id.exists' => 'Incorrect or Invalid user id'
        ];
        $validator = Validator::make($input, $rules,$messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
        } else {
            $id = $input['id'];
            $user = $this->_repository->findById($id);
            if ($user == NULL) {
                $code = 404;
                $output = ['error' => ['code' => $code,'messages' => ['User not found']]];
            } else {
                if($user->visibility == 1){

                    unset($user->updated_at, $user->deleted_at, $user->role_id, $user->access_token,$user->recover_password_key,$user->recover_attempt_at,$user->create_password);
                    $code = 200;
                    $output =['response' => ['code' => $code,'data' => $user]];
                }else{
                    $code = 406;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'This user account has been blocked.' ] ] ];
                }
            }
        }
        return response()->json($output, $code);
    }

    public function forgotPassword(Request $request) {

        $input['email']     = $request->request->get('email');
        
        $rules = [
                 'email' => 'required|email|exists:users,email'
                 ];

        $messages = [
                    'email.required' => 'Please enter email address.',
                    'email.exists'  => 'Incorrect or invalid email address.'
                    ];

        $validator = Validator::make($input,$rules,$messages);

        if($validator->fails()) {

            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all() ]];
        }
        else {

            $data = $this->_repository->findByAttribute('email', $input['email'], false, true, false);
            if($data != NULL) {

                $key = Str::random(60);

                
                $updatedUser = $this->_repository->update([
                    'id' => $data->id,
                    'recover_attempt_at' => Carbon::now(),
                    'recover_password_key' =>  $key,
                ]);

                if($updatedUser == NULL) {

                    $code = 406;
                    $output = ['error' => ['code'=>$code, 'messages' => ['An error occurred while trying to reset your password. Please try again.']]];

                } else {
                    $code = 200;
                    $output = ['response' => ['code'=> $code, 'messages' => ['A password reset link has been sent to your account. Please check your inbox.']]];
                    Event::fire(new PasswordWasRecovered($updatedUser));
                }

            } else {
                $code = 404;
                $output = ['error'=> ['code' => $code, 'messages' => ['Incorrect or invalid email address.']]];
            }
        }

        return response()->json($output, $code);
    }

    public function resetPassword(Request $request) {

        $input['recover_password_key']  = $request->input('recover_password_key');
        $input['password']              = $request->request->get('password');
        $input['confirm_password']      = $request->request->get('confirm_password');

        $rules = [
                 'recover_password_key' => 'required',
                 'password'             => 'required|min:6',
                 'confirm_password'     => 'required | same:password|min:6'
                 ];

        $messages = [
                    'recover_password_key.required' => 'Please enter recover password key.',
                    'password.required'             => 'Please enter password.',
                    'confirm_password.required'     => 'Please enter confirm password.'
                    ];

        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {

            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all() ]];

        } else {

            $data = $this->_repository->findByAttribute('recover_password_key', $input['recover_password_key'], false, true, false);
           

            if($data != NULL) {

                $updatedUser = $this->_repository->update([
                    'id'        => $data->id,
                    'password'  => Hash::make($input['password']),
                    'recover_password_key' =>  '',
                ]);
               
                if($updatedUser == NULL) {

                    $code = 406;
                    $output = ['error' => ['code'=> $code, 'messages' => ['Error in reseting password.']]];

                } else {

                    $code = 200;
                    // $output = ['response' =>['code'=> $code, 'messages' => ['Your password has been reset successfully.']]];
                    $users['code'] = $code;
                    $output['response'] = $updatedUser;
                    Event::fire(new PasswordWasReset($updatedUser));
                }

            } else {
                $code = 404;
                $output = ['error' => ['code'=>$code, 'messages' => ['Incorrect or invalid recover password key.']]];
            }
        }

        return response()->json($output, $code);
    }

    
    public function userSearch(Request $request) {
       
        $input = $request->only('keyword');
        
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
        }
    
        $rules = [
                'user_id'   =>  'required|exists:users,id',
                'keyword'   =>  'required|string',
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {
            $response = $this->_repository->userSearch(false, 10, $input);
            if($response){
                $code = 200;
                $output = ['response' => ['code' => $code, 'data' => $response]];
            } else {
                $code = 400;
                $output = ['error' => ['code' => $code, 'messages' => ['An error occurred while fetching data.']]];
            }
        }
       

        return response()->json($output, $code);
    }
    
    public function upload(Request $request) {

        $input = $request->only('file','id','type');
        $input["type"]="simple";
        $input = array_filter($input,'strlen');
        if (isset($input['id'])) {
            $id = hashid_decode($input['id']);    
        }
        
        $token = JWTAuth::getToken();
        if($token){
                $claims = JWTAuth::decode($token);
                if($claims instanceof Payload && $claims->get('sub')) {
                    $input['id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        if (isset($id) && $id != NULL) {
            $input['id'] = $id;
        }
        $rules = [
            'id'   =>  'required|exists:users,id',
            'file' => 'required|mimes:jpeg,jpeg,png',
            'type' => 'required|in:profile,collage,simple'
        ];

        $messages = [
        ];
        //dd(config('app.files.users.folder_name'));
        $validator = Validator::make($input, $rules, $messages);
            
        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
        } else {
            if($input['type'] == 'simple'){
                $file_name = $input['file']->store(config('app.files.users.folder_name'));
                $data['id'] = $input['id'];
                $data['profile_pic'] = $input['file']->hashName();
                    
            }else{
                $file_name = $input['file']->store(config('app.files.collage.folder_name'));
                $data['id'] = $input['id'];
                $data['dream_collage'] = $input['file']->hashName();
                
            }
            $profilePic = $this->_repository->update($data,true,true);
            if ($file_name && $this->crop($input['file'],$input['type'])) {

                $code = 200;
                $output = ['response' => ['code' => $code, 'messages' => ['File saved successfully'], 'data' => $profilePic]];
            } else {
                $code = 500;
                $output = ['error' => ['code' => $code, 'messages' => ['Unable to save file.']]];
            }
        }

        // all good so return the token
        return response()->json($output, $code);
    }

    private function crop($file,$type = 'simple', $extensions = ['jpeg', 'png', 'gif', 'bmp', 'svg']) {

        list($name, $ext) = explode('.', $file->hashName());
        if (in_array($file->guessExtension(), $extensions)) {
            $image = Image::make($file);
            $width = $image->width();
            $height = $image->height();
            if ($type == 'simple') {
                $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'@3x.'.$ext, $image->stream());
                $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'@2x.'.$ext, $image->resize($width / 1.5, $height / 1.5)->stream());
                $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'.'.$ext, $image->resize($width / 3, $height / 3)->stream());
            } else{
                $store = Storage::put(config('app.files.collage.folder_name').'/'.$name.'.'.$ext, $image->stream());
            }
            
        }
        return true;
    }

    public function check(Request $request) {

        $input = $request->only('username');
        $rules = [
                    'username'     => 'required'
                ];
        // $messages = [
            
        // ];
        //dd($input);
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
                $code = 404;
                $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
        } else {
                $user = User::where("username","=",$input['username'])->orWhere("email","=",$input['username'])->first();
                if($user == NULL){
                    $code = 200;
                    $output = ['response' => ['code' => $code, 'messages' => "Email and Username Available."]];
                }else{
                    $code = 404;
                    $output = ['error' => ['code' => $code, 'messages' => "Email or Username not Available."]];
                }
                
        }
        return response()->json($output, $code);
    }

    public function setVisibility(Request $request){
        $input = $request->only('id','is_visible');
        $input['id'] = hashid_decode($input['id']);
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
        }
        $rules = [
                    'user_id'     => 'required|exists:users,id',
                    'id'     => 'required|exists:users,id',
                    'is_visible' => 'required'
                ];
        $messages = [
            'id.exists' => 'Incorrect or Invalid user id.',
            'user_id.exists' => 'You are not allow to access this.',
        ];
        $validator = Validator::make($input, $rules,$messages);

        if ($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];
        } else {
                $visibility = $this->_repository->setUserVisibility($input);
                if($visibility){
                    $code = 200;
                    $output = ['success' => ['code' => $code, 'messages' => "Successfully updated"]];
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['An error occurred while updating data.']]];
                }
                
        }
        return response()->json($output, $code);
    }

  
    // public function connectSocialAccount(Request $request){

    //     $input = $request->only('fb_access_token');
        
    //     $token = JWTAuth::getToken();
    //     if($token){
    //         $claims = JWTAuth::decode($token);
    //         if($claims instanceof Payload && $claims->get('sub')) {
    //             $input['user_id'] = $claims->get('sub');        
    //         } else {
    //                 $code = 400;
    //                 $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
    //                 return response()->json($output, $code);
    //             }
    //     }
    
    //     $rules = [
    //             'user_id'   =>  'required|exists:users,id',
    //             'fb_access_token'   =>  'required',
    //     ];

    //     $validator = Validator::make($input, $rules);
    //     if ($validator->fails()) {
    //         $code = 406;
    //         $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
    //     } else {
    //         try {
    //             $user = Socialite::driver('facebook')
    //                 ->userFromToken($input['fb_access_token']);
    //             $input['social_network'] = 'facebook';
    //             $input['social_network_id'] = $user->id;
    //             $response = $this->_repository->connectSocialAccount($input);
    //             if($response === 'current_already_connected'){
    //                 $code = 406;
    //                 $output = ['error' => ['code' => $code, 'messages' => ['This social account is already connected with this account.']]];
    //             } else if($response === 'already_connected'){
    //                 $code = 406;
    //                 $output = ['error' => ['code' => $code, 'messages' => ['Another Social account is already connected.']]];
    //             } else if($response !== false) {
    //                 unset($response->password,  $response->recover_password_key,$response->recover_attempt_at, $response->updated_at, $response->deleted_at, $response->role_id,$response->created_at,$response->access_token);                    
    //                 $code = 200;
    //                 $output = ['response' => ['code' => $code, 'data' => $response, 'messages' => ['You have successfully connected your social account.']]];
    //             } else {
    //                 $code = 406;
    //                 $output = ['error' => ['code' => $code, 'messages' => ['An error occurred while retriving your facebook details Please try again.']]];
    //             }
    //         } catch(\GuzzleHttp\Exception\ClientException $e){
    //             $code = 401;
    //             $output = ['error' => [ 'code' => $code, 'messages' => [ 'Looks like your facebook access token has expired or is invalid.' ] ] ];
    //         } catch(\GuzzleHttp\Exception\ServerException $e){
    //             $code = 401;
    //             $output = ['error' => [ 'code' => $code, 'messages' => [ 'Looks like your facebook access token has expired or is invalid.' ] ] ];
    //         } catch(Exception $e){
    //             $code = 401;
    //             $output = ['error' => [ 'code' => $code, 'messages' => [ 'Looks like your facebook access token has expired or is invalid.' ] ] ];
    //         }
    //     }
    //     return response()->json($output, $code);        
    // }

    // public function disconnectSocialAccount(Request $request){

    //     $input = [];
        
    //     $token = JWTAuth::getToken();
    //     if($token){
    //         $claims = JWTAuth::decode($token);
    //         if($claims instanceof Payload && $claims->get('sub')) {
    //             $input['user_id'] = $claims->get('sub');        
    //         } else {
    //                 $code = 400;
    //                 $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
    //                 return response()->json($output, $code);
    //             }
    //     }
    
    //     $rules = [
    //             'user_id'   =>  'required|exists:users,id',
    //     ];

    //     $validator = Validator::make($input, $rules);
    //     if ($validator->fails()) {
    //         $code = 406;
    //         $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
    //     } else {

    //         $response = $this->_repository->disconnectSocialAccount($input);
    //         if($response === 'not_connected'){
    //             $code = 406;
    //             $output = ['error' => ['code' => $code, 'messages' => ['Social account needs to be connected first.']]];
    //         } else if($response !== false) {
    //             unset($response->password,  $response->recover_password_key,$response->recover_attempt_at, $response->updated_at, $response->deleted_at, $response->role_id,$response->created_at,$response->access_token);                    
    //             $code = 200;
    //             $output = ['response' => ['code' => $code, 'data' => $response, 'messages' => ['You have successfully connected your social account.']]];
    //         } else {
    //             $code = 406;
    //             $output = ['error' => ['code' => $code, 'messages' => ['An error occurred while retriving your facebook details Please try again.']]];
    //         }

    //     }
    //     return response()->json($output, $code);        
    // }

    public function refreshToken(Request $request) {
        $token = JWTAuth::getToken();
            if($token){
                $claims = JWTAuth::decode($token);
                if($claims instanceof Payload && $claims->get('sub')) {
                    $input['user_id'] = $claims->get('sub');        
                } else {
                    $code = 400;
                    $output = ['error' => ['code' => $code, 'messages' => ['Invalid Token.']]];
                    return response()->json($output, $code);
                }
            }
        $rules = ['user_id' => 'required|exists:users,id'];
        $messages = [];

        $validator = Validator::make($input, $rules, $messages);

        // if validation fails
        if($validator->fails()) {
            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all()]];

        // if validation passes
        } else {
            $userData = $this->_repository->findById($input['user_id'],true,true,false,false); 
            $token = JWTAuth::fromUser($userData);
            $users =  $this->_repository->update([
                                    'id' => $input['user_id'],
                                    'login_at' => Carbon::now(),
                                    'access_token' => $token,
                                ],true,true);
            $code = 200;
            $output['code'] = $code;
            $output['response']['data'] = $token;
        }
        return response()->json($output);
    }

    public function remove(Request $request) {
           
        $input = $request->only('id');
        $input['id'] = hashid_decode($input['id']);

        $rules = ['id' => 'required|exists:users,id'];

        $messages = ['id.required' => 'Please enter user id'];

        $validator = Validator::make( $input, $rules, $messages);

        if($validator->fails()){
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>$validator->messages()->all()]];

        } else{

            $user = $this->_repository->deleteById($input['id']);
            if($user == false){
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occurred while deleting this user.']]];

            } else {
                $code = 200;
                $output = ['response'=>['code'=>$code,'messages'=> ['User has been deleted successfully.']]];
            }
        }

        return response()->json($output, $code);
    }     

    public function addUserView(Request $request) {
        
        $input = $request->only('profile_id');
        $input['profile_id'] = hashid_decode($input['profile_id']);
        
        $token = JWTAuth::getToken();
        if($token){
            $claims = JWTAuth::decode($token);
            if($claims instanceof Payload && $claims->get('sub')) {
                $input['user_id'] = $claims->get('sub');        
            }
        }

        $rules = [
            'profile_id'    =>  'required|exists:users,id,deleted_at,NULL',
            'user_id'    =>  'required|exists:users,id,deleted_at,NULL',
         ];

        $validator = Validator::make($input,$rules);

        if($validator->fails()) {

            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        } else {

            $input['type'] = 'view';
            $checkProfileView = UserProfileAction::where("profile_id","=",$input['profile_id'])->where("user_id","=",$input['user_id'])->where("type","=","view")->first();
            if($checkProfileView == NULL){
                $profileView = $this->_repository->createProfileView($input);
                if($profileView == NULL) { 
                    $code = 406;
                    $output = ['error' => ['code'=>$code, 'messages' => ['An error occurred while adding record.']]];
                } else {
                    $code = 200;
                    $output = ['response' => ['code'=> $code, 'messages' => ['Profile view saved successfully.']]];
                    $profileData['total_views'] = UserProfileAction::where("profile_id","=",$input['profile_id'])->where("type","=","view")->count();
                    $output['response']['data'] = $profileData;
                }
            }else{
                $code = 408;
                $output = ['error' => ['code'=>$code, 'messages' => ['Profile Already Viewed.']]];
            }
           
            
            
        }

        return response()->json($output, $code);
    }     
    public function feedback(Request $request)
    {
        $input = $request->only('user_id', 'feedback');
        $input = array_filter($input,'strlen');

        //dd($input);
        
        $rules = [
            'user_id'                        =>  'required|exists:users,id',
            'feedback'              =>  'required|max:255'
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {            
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            //dd($output);
        } else {
            if( isset($input['user_id']) ){
                $feedback_exist = Feedback::where("user_id",$input['user_id'])->first();
                //dd($feedback_exist);
                if($feedback_exist != NULL){
                    $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'Feedback has already been submitted.' ] ] ];
                    return response()->json($output, $code);
                }
            }
            $feedback_obj = new Feedback();
            $feedback_obj->user_id = $input['user_id'];
            $feedback_obj->feedback = $input['feedback'];
            if($feedback_obj->save())
            {
                $code = 200;
                    $output = ['response' => [ 'code' => $code, 'messages' => [ 'Feedback submitted successfully.' ] ] ]; 
            }
            else
            {
                $code = 401;
                    $output = ['error' => [ 'code' => $code, 'messages' => [ 'Feedback has already been submitted.' ] ] ];
            }
        }        
        return response()->json($output, $code);
    }
public function changePassword(Request $request) {

       
        $input = $request->only('password','new_password','confirm_password','user_id');
        
        $rules = [
                 'new_password' => 'required',
                 'password'             => 'required|min:6',
                 'confirm_password'     => 'required | same:new_password|min:6'
                 ];

        $messages = [
                    'new_password'             => 'Please enter new password.',
                    'password.required'             => 'Please enter password.',
                    'confirm_password.required'     => 'Please enter confirm password.'
                    ];



        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {

            $code = 406;
            $output = ['error' => ['code' => $code, 'messages' => $validator->messages()->all() ]];

        } else {
          
            $data= User::find($input['user_id']);           

            if($data != NULL && Hash::check($input['password'], $data->password)) {                
                $updatedUser = $this->_repository->update([
                    'id'        => $data->id,
                    'password'  => Hash::make($input['new_password'])                    
                ]);            
               
                if($updatedUser == NULL) {

                    $code = 406;
                    $output = ['error' => ['code'=> $code, 'messages' => ['Error in changing password.']]];

                } else {

                    $code = 200;                    
                    $users['code'] = $code;
                    $res['data'] = $updatedUser;
		$res['code'] = $code;
		$res['messages'] = ['Password changed Successfully.'];
		$output = ['response' => $res];


                    //Event::fire(new PasswordWasReset($updatedUser));
                }

            } else {
                $code = 404;
                $output = ['error' => ['code'=>$code, 'messages' => ['Incorrect or invalid password entered.']]];
            }
        }

        return response()->json($output, $code);
    }
}
