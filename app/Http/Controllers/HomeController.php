<?php

namespace receipt\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
// use Socialite;
use receipt\Http\Controllers\Controller;
use Illuminate\Support\Facades\Event;
use receipt\Data\Models\UserSocialAccount;
use receipt\Data\Repositories\UserRepository;
use receipt\Data\Repositories\FolderRepository;
use receipt\Data\Models\User;
use receipt\Data\Models\UserFolder;
use receipt\Data\Models\Feedback;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Image,Storage,App;

class HomeController extends Controller {
    const PER_PAGE = 10;
    private $_repository;

    public function __construct(UserRepository $user,FolderRepository $folder) {
        $this->_repository = $user;
        $this->_folderRepo = $folder;
    }



    public function login(Request $request) {

        $input = $request->only('email', 'password');   

        $rules['email'] = 'required|email|exists:users,email,is_admin,1';
        $rules['password'] = 'required';
        

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $code = 406;
            // $output = ['messages' => $validator->messages()->all() ];
            $output = 'Invalid Email and Password';
            // dd($output);
        } else {

            $user = $this->_repository->login($input);
            if ($user) {
                $code = 200;
                $output = [
                'response' => [
                'code' => $code,
                'message'=>['You have been logged in successfully'],
                ]
                ];
                unset($user->password,  $user->recover_password_key,$user->recover_attempt_at, $user->updated_at, $user->deleted_at, $user->created_at);
                $output['response']['data'] = $user;

                $request->session()->put('key',$user);

                return Redirect('/users');

            } else {
                $code = 401;
                // $output = ['error' => [ 'code' => $code, 'messages' => [ 'The email and password do not match' ] ] ];
                $output = 'The email and password do not match' ;
            }
        }
        $request->session()->flash('status',$output);
        return Redirect('/');

        // return response()->json($output, $code);
    }

    private function crop($file,$type = 'simple', $extensions = ['jpeg', 'png', 'gif', 'bmp', 'svg']) {

        list($name, $ext) = explode('.', $file->hashName());
        if (in_array($file->guessExtension(), $extensions)) {
            $image = Image::make($file);
            $width = $image->width();
            $height = $image->height();
            if ($type == 'simple') {
                $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'@3x.'.$ext, $image->stream());
                $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'@2x.'.$ext, $image->resize($width / 1.5, $height / 1.5)->stream());
                $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'.'.$ext, $image->resize($width / 3, $height / 3)->stream());
            } else{
                $store = Storage::put(config('app.files.collage.folder_name').'/'.$name.'.'.$ext, $image->stream());
            }
            
        }
        return true;
    }

    public function remove(Request $request) {

        $input = $request->only('id');
        $input['id'] = hashid_decode($input['id']);

        $rules = ['id' => 'required|exists:users,id'];

        $messages = ['id.required' => 'Please enter user id'];

        $validator = Validator::make( $input, $rules, $messages);

        if($validator->fails()){
            $code = 406;
            $output = ['error'=>['code'=>$code,'messages'=>$validator->messages()->all()]];

        } else{

            $user = $this->_repository->deleteById($input['id']);
            if($user == false){
                $code = 406;
                $output = ['error'=>['code'=>$code,'messages'=>['An error occurred while deleting this user.']]];

            } else {
                $code = 200;
                $output = ['response'=>['code'=>$code,'messages'=> ['User has been deleted successfully.']]];
            }
        }

        return response()->json($output, $code);
    }     

    public function showUsers(Request $request){

        $input = $request->only('name');

        if (isset($input['name'])) {
            $userdata = User::where('users.is_admin','0')->whereRaw('users.first_name like  "%'.$input['name'].'%" OR users.last_name like "%'.$input['name'].'%"')
            ->orderBy('users.id','DESC')
            ->paginate(10);

        }else{
            $userdata = User::where('users.is_admin','0')
            ->orderBy('users.id','DESC')
            ->paginate(10);
        }


        

        if ($userdata) {
            $i = 0;
            // $userRepo = App::make('UserRepository');
            foreach ($userdata as $key => $value) {
                $users =  $this->_repository->findById($value->id);

                $userdata[$i]->primary_id = hashid_encode($value->id);
                $userdata[$i]->image_urls = $users->image_urls;
                $userdata[$i]->folder_count = UserFolder::where('user_id',$value->id)->count();


                unset($userdata[$i]->id);
                $i++;
            }
        }else{
            $users = [];
        }
        if (!isset($input['name'])) {
            return view('admin.users',['data' => $userdata]);
        }
        else {
            $code = 200;
            $output = ['response' => ['code' => $code, 'data' => $userdata]];      
            return response()->json($output, $code);
        }
    }

    public function showFolders(Request $request){

        $input = $request->only('name');

        if (isset($input['name'])) {
            $userdata = UserFolder::where('users.is_admin','0')->whereRaw('users.first_name like  "%'.$input['name'].'%" OR users.last_name like "%'.$input['name'].'%"')
            ->orderBy('users.id','DESC')
            ->paginate(10);

        }else{
            $userdata = UserFolder::where('users.is_admin','0')
            ->orderBy('users.id','DESC')
            ->paginate(10);
        }


        

        if ($userdata) {
            $i = 0;
            foreach ($userdata as $key => $value) {
                $users =  $this->_folderRepo->findById($value->id);

                $userdata[$i]->primary_id = hashid_encode($value->id);
                $userdata[$i]->image_urls = $users->image_urls;
                unset($userdata[$i]->id);
                $i++;
            }
        }else{
            $users = [];
        }
        if (!isset($input['name'])) {
            return view('admin.folders',['data' => $userdata]);
        }
        else {
            $code = 200;
            $output = ['response' => ['code' => $code, 'data' => $userdata]];      
            return response()->json($output, $code);
        }
    }

    public function ChangeActive(Request $request){

        $input = $request->only('id');
        // print_r(Helper::hashid_decode($input['id']));
        // die();
        $user = hashid_decode($input['id']);
        $user = User::where('id',hashid_decode($input['id']))->first();
        if ($user->is_block) {
            $user = User::where('id',hashid_decode($input['id']))->update(['is_block' => 0]);
            $output = 'deactive';
        }else{
            $user = User::where('id',hashid_decode($input['id']))->update(['is_block' => 1]);
            $output = 'active';
        }
            $code = 200;
            $output = ['response' => ['code' => $code, 'data' => $output]];      
            return response()->json($output, $code);
    }

    public function update(Request $request){

        $input = $request->only('password','old_password');

        $input['id'] = $request->session()->get('key')->id;

        $user = User::where('id',hashid_decode($input['id']))
        ->where('is_admin',1)->first();
        // dd($user);

        if ($user) {
            if (Hash::check($input['old_password'],$user->password)) {
                $data['id'] = hashid_decode($input['id']);
                $data['password'] = Hash::make($input['password']);
                $updated = $this->_repository->update($data);
                if ($updated) {

                    $request->session()->flash('update','Update successfully!');
                    return Redirect('/settings');
                }
            }
        }
       $request->session()->flash('error','Error! Wrong Password!');
        return Redirect('/settings');
    }


}