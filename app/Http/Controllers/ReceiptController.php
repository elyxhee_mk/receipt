<?php

namespace receipt\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Image,Storage;
use receipt\Data\Models\UserReceipt;
use receipt\Data\Repositories\ReceiptRepository;
use Validator;

class ReceiptController extends Controller
{
   const PER_PAGE = 10;
   private $_repository;

    public function __construct(ReceiptRepository $repo) {
        
        $this->_repository = $repo;
        $this->per_page = 10;        
    }
    public function add(Request $request) {

    	
    	$input = $request->only('user_id','doc_name','notes','folder_id','doc_type','receipt_date','expiry_date','content','gift_card_value');
        $input['folder_id'] = hashid_decode($input['folder_id']);
             
        $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',
            'doc_name'             =>  'required|max:50',
            'notes'       =>  'sometimes|max:255',            
            'doc_type'         =>  'required|in:receipt,coupon,giftcard',
            'folder_id'             =>  'required|exists:user_folders,id,deleted_at,NULL',
            'content'       => 'required|mimetypes:image/jpeg'
            
        ]; 
	if(isset($input['doc_type']) && $input['doc_type'] == "giftcard")
            $rules['gift_card_value'] = 'required|numeric|min:0'; 
        else
            unset($input['gift_card_value']) ;        
        
        $validator = Validator::make($input, $rules);        
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                //dd($validator->messages()->messages());
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
        	unset($input["user_id"]);          
            
            if($input['content'] != NULL){
                //dd($input['content']);
                $mime = $request->file('content')->getMimeType();
                $extension = $request->file('content')->getClientOriginalExtension();
                if($mime == 'image/png' || $mime == 'image/jpg'|| $mime == 'image/jpeg'||$mime == 'image/gif'){
                    $input['type'] = 'photo';
                }else{
                    $input['type'] = 'video';
                }
                $input['file_type'] = $mime;
                $input['original_name'] = $request->file('content')->getClientOriginalName();
            } 
            $response = $this->_repository->create($input);
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
				'messages'=>['Document Added Successfully.'],
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating course.']]];
                    }
        }

        return response()->json($output, $code);
    }
    public function update(Request $request) {

    	 
    	$input = $request->only('user_id','doc_name','notes','folder_id','doc_type','receipt_date','expiry_date','content','id','gift_card_value');
        $input = array_filter($input);
        if(isset($input['id']))
            $input['id'] = hashid_decode($input['id']);
        if(isset($input['folder_id']))
            $input['folder_id'] = hashid_decode($input['folder_id']);
        //dd( $input);
         $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',
            'doc_name'             =>  'sometimes|max:50',
            'notes'       =>  'sometimes|max:255',            
            'doc_type'         =>  'sometimes|in:receipt,coupon,giftcard',
            'folder_id'             =>  'sometimes|exists:user_folders,id,deleted_at,NULL' ,
            'id'  =>   'required|exists:user_receipts,id,deleted_at,NULL'           
            
        ];
	if(isset($input['doc_type']) && $input['doc_type'] == "giftcard")
            $rules['gift_card_value'] = 'required|numeric|min:0';
        else
            unset($input['gift_card_value']) ;    

        $validator = Validator::make($input, $rules);
        
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                //dd($validator->messages()->messages());
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
        	//dd($input);
        	unset($input['user_id']);
        	unset($input['content_id']);
            //dd($input);     
            $response = $this->_repository->update($input);
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
				'messages'=>['Document Updated Successfully.'],
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating course.']]];
                    }
        }

        return response()->json($output, $code);
    }

    public function all(Request $request)
    {
    	$input = $request->only('user_id','id','doc_type','folder_id','pagination');
        $input = array_filter($input);
        if(isset($input["id"]) && $input["id"] != "")
            $input["id"] = hashid_decode($input["id"]);
        if(isset($input["folder_id"]) && $input["folder_id"] != "")
            $input["folder_id"] = hashid_decode($input["folder_id"]);

         $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1'
            ];
    	if(isset($input['id']) && $input['id'] != "")
    	{
        	$rules['id'] = 'required|exists:user_receipts,id,deleted_at,NULL';        	
    	}
        if(isset($input['folder_id']) && $input['folder_id'] != "")
        {
            $rules['folder_id'] = 'required|exists:user_folders,id,deleted_at,NULL';          
        }
    	if(isset($input['doc_type']) && $input['doc_type'] != "")
    	{
        	$rules['doc_type'] = 'required|in:receipt,coupon,giftcard';        	
    	}
        //dd($input);
        $validator = Validator::make($input, $rules);
       
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
        	      
            $this->_repository->setEncodedKeys(array("folder_id"));
            if(isset($input['id']) && $input['id'] != ""){
            	$response = $this->_repository->findById($input['id']);
            }
            else{
            	$pagination = true;
                if(isset($input['pagination']) && $input['pagination'] == "false")
                    $pagination = false;                
                $response = $this->_repository->findByAll($pagination,$this->per_page,$input);
            }
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
				'messages'=>['Document List.'],
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating course.']]];
                    }
        }

        return response()->json($output, $code);
    }
    public function getDetail(Request $request)
    {
        $input = $request->only('user_id','course_id');
        $input['course_id'] = hashid_decode($input['course_id']);
       

        $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',
            'course_id'             =>  'required|exists:contents,id,deleted_at,NULL',
        ];
        $validator = Validator::make($input, $rules);
        //dd($rules);
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                //dd($validator->messages()->messages());
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            //dd($input);
            
            $response = $this->_repository->getCourseDetail($input['course_id'],"",true);
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
			'messages'=>['Document list.'],
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating course.']]];
                    }
        }

        return response()->json($output, $code);

    }
    public function delete(Request $request)
    {
      $input = $request->only('user_id','id'); 
     // $input['id'] = hashid_decode($input['id']);
      $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',            
            //'id'  =>   'required|exists:user_receipts,id,deleted_at,NULL'           
            
        ];
    $validator = Validator::make($input, $rules);
    
    if ($validator->fails()) {
        if(array_key_exists("user_id", $validator->messages()->messages())){
            //dd($validator->messages()->messages());
            $code = 401;
            $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
        }else{
            $code = 406;
            $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
        }
    } else {
	$docs = array(); 
        $response = true;                     
        if(count($input['id']) > 0)
        {
            $ids_deleted = array();
            foreach($input['id'] as $k => $doc_id)
            {
                 
                 $doc=UserReceipt::find(hashid_decode($doc_id));                
                 if($doc != NULL)
                {
                   $ids_deleted[] = $doc_id;
                    $response = $doc->delete();
                }
            }
        }
           
        //$response = $this->_repository->deleteById($input['id']);
        if ($response) {
                $code = 200;
                $output = [
                        'response' => [
                        'code' => $code,
			'messages'=>['Document Deleted Successfully.'],

                        'data' => $ids_deleted,
                        ]
                    ];
                }else{
                    $code = 409;
                    $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating course.']]];
                }
        }

        return response()->json($output, $code); 
    }
        
    
}
