<?php

namespace receipt\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Image,Storage;
use receipt\Data\Models\UserFolder;
use receipt\Data\Repositories\FolderRepository;
use Validator;

class FolderController extends Controller
{
   const PER_PAGE = 10;
   private $_repository;

    public function __construct(FolderRepository $repo) {
        
        $this->_repository = $repo;
        $this->per_page = 10;        
    }
    public function add(Request $request) {

    	$input = $request->only('user_id','name');

        $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',
            'name'             =>  'required|max:50'       
            
        ];
        $validator = Validator::make($input, $rules);        
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                //dd($validator->messages()->messages());
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {        	    
            $response = $this->_repository->create($input);
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
				'messages'=>['Folder Created Successfully.'],
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating folder.']]];
                    }
        }

        return response()->json($output, $code);
    }
    public function rename(Request $request) {

    	 
    	$input = $request->only('id','user_id','name');
        $input['id'] = hashid_decode($input['id']);


        $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',
            'name'             =>  'required|max:50',
            'id'         =>  'required|exists:user_folders,id,deleted_at,NULL'           
            
        ];
        $validator = Validator::make($input, $rules);
        //dd($rules);
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                //dd($validator->messages()->messages());
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            //dd($input);
        	unset($input['user_id']);        	     
            $response = $this->_repository->update($input);
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
				'messages'=>['Folder Rename Successfully.'],
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating course.']]];
                    }
        }

        return response()->json($output, $code);
    }
    public function delete(Request $request) {

         
        $input = $request->only('id','user_id');
        //$input['id'] = hashid_decode($input['id']);
        //dd($input);


        $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',            
            //'id'         =>  'required|exists:user_folders,id,deleted_at,NULL'           
            
        ];
        $validator = Validator::make($input, $rules);
        //dd($rules);
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                //dd($validator->messages()->messages());
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
            
            
            //$response = $this->_repository->delete($input['id']);
		$folders = array(); 
            $response = true;                     
            if(count($input['id']) > 0)
            {
                foreach($input['id'] as $k => $folder_id)
                {
                     $folder=UserFolder::find(hashid_decode($folder_id));
                     if($folder != NULL)
                        $response = $folder->delete();
                }
            }

            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
				'messages'=>['Folder Deleted Successfully.'],
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while deleting Folder.']]];
                    }
        }

        return response()->json($output, $code);
    }

    public function all(Request $request)
    {
    	$input = $request->only('user_id','id');
        $input = array_filter($input);
        if(isset($input['id']))
    	   $input['id'] = hashid_decode($input['id']); 

        //dd($input); 

    	$rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',
            'id'             =>  'sometimes|exists:user_folders,id,deleted_at,NULL',
            

        ];
        
        $validator = Validator::make($input, $rules);
       
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {
        	      
            if(isset($input['id']) && $input['id'] != ""){
            	$response = $this->_repository->findById($input['id']);
            }
            else{
            	$response = $this->_repository->findByAll(true,$this->per_page,$input);
            }
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
				'messages'=>['Folder List.'],
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating course.']]];
                    }
        }

        return response()->json($output, $code);
    }
    public function getDetail(Request $request)
    {
        $input = $request->only('user_id','course_id');
        $input['course_id'] = hashid_decode($input['course_id']);
       

        $rules = [
            'user_id'             =>  'required|exists:users,id,visibility,1',
            'course_id'             =>  'required|exists:contents,id,deleted_at,NULL',
        ];
        $validator = Validator::make($input, $rules);
        //dd($rules);
        if ($validator->fails()) {
            if(array_key_exists("user_id", $validator->messages()->messages())){
                //dd($validator->messages()->messages());
                $code = 401;
                $output = ['error' => [ 'code' => $code, 'messages' => 'You dont have permission to perform this action, You may be removed or disabled by admin.' ] ];
            }else{
                $code = 406;
                $output = ['error' => [ 'code' => $code, 'messages' => $validator->messages()->all() ] ];
            }
        } else {           
            
            $response = $this->_repository->getCourseDetail($input['course_id'],"",true);
            if ($response) {
                    $code = 200;
                    $output = [
                            'response' => [
                            'code' => $code,
                            'data' => $response,
                            ]
                        ];
                    }else{
                        $code = 409;
                        $output = ['error'=>['code'=>$code,'messages'=>['An error occured while creating course.']]];
                    }
        }

        return response()->json($output, $code);

    }
        
    
}
