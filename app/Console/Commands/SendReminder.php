<?php

namespace receipt\Console\Commands;

use Illuminate\Console\Command;

use receipt\Data\Models\UserReceipt;
use receipt\Events\CouponExpiryWasGenerated;

class SendReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Reminder:Send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Will Send remiders for the user coupon Expiry';

    /**
     * Create a new command instance.
     *
     * @return void
     */    

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        //dd("send Reminder");
        $couponsToRemind=UserReceipt::join('user_folders','user_receipts.folder_id','=','user_folders.id')->where("doc_type","coupon")->whereRaw('expiry_date > now() and DATE_SUB(expiry_date,INTERVAL 6 HOUR) < NOW()')->where("sent_reminder",0)->get(['user_receipts.id','user_receipts.doc_name','user_receipts.expiry_date','user_folders.user_id']);

        //dd("for notification ",count($couponsToRemind));
        if(count($couponsToRemind) > 0)
        {
            foreach($couponsToRemind as $key => $coupon)
            {
                $coupon_det = array();
                $coupon_det["id"] = $coupon->id;
                $coupon_det["coupon_name"] = $coupon->doc_name;
                $coupon_det["expiry_date"] = $coupon->expiry_date; 
                $coupon_det["user_id"] = $coupon->user_id; 
                //dd("coupon detail",$coupon_det);                              
                if(event(new CouponExpiryWasGenerated($coupon_det)))
                    UserReceipt::where("id",$coupon->id)->update(['sent_reminder'=>0]);
                
            }
        }        
    }
}
