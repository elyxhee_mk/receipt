<?php
namespace receipt\Listeners;

use receipt\Events\SendUserActivation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;

class SendUserActivationConfirmation implements ShouldQueue
{
     public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }   

    /**
     * Handle the event.
     *
     * @param  Registration  $event
     * @return void
     */
    public function handle(SendUserActivation $event)
    {
        
        $user = $event->user;
        $name = $user->first_name.' '.$user->last_name;
        $this->mailer->send('emails.registartion', ['user' => $user], function ($m) use ($user,$name) {
            $m->to($user->email, $name)->subject('Registration');
        });
    }

    public function queue($queue, $command, $data) {
      $queueName = 'tree_user_registration_'.config('app.queue_registration_fix');
      $queue->pushOn($queueName, $command, $data);
   }
}
