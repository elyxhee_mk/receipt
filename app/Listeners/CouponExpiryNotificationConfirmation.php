<?php
namespace receipt\Listeners;

use receipt\Events\CouponExpiryWasGenerated;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
use receipt\Data\Models\DeviceToken;
use receipt\Data\Models\User;
use receipt\Data\Models\UserReceipt;
use receipt\Support\Helper;
use receipt\Data\Models\Activity;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class CouponExpiryNotificationConfirmation implements ShouldQueue
{
     public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }   

    /**
     * Handle the event.
     *
     * @param  Registration  $event
     * @return void
     */
    
    public function handle(CouponExpiryWasGenerated $couponEvent)
    {
        
        $coupon= $couponEvent->coupon;
        $success = false;
        
        
        $title = $coupon["coupon_name"].' is expiring today by'.$coupon["coupon_name"];
        
        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20)->setPriority('high')->setContentAvailable(true);
        
        $view = Helper::PHOTO_CREATED;
        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setTitle($title)
                            ->setSound(1)
                            ->setBody($title)
                            ->setClickAction('MY_KEY_NOTIFICATION');

        $dataBuilder = new PayloadDataBuilder();
        
        $custom['view'] = $view;
        $custom['view_id'] = $coupon['id'];
        $custom['type'] = 'coupon';        
          
        $dataBuilder->addData(['custom' => $custom]);
        $dataBuilder->setData(['custom' => $custom]);
        
        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();

        $data = $dataBuilder->build();
        //dd($data);

        //$deviceToken = DeviceToken::join("users","device_token.user_id","=","users.id")->where("users.id",$coupon["user_id"])->get(['device_token.token','device_token.user_id']);
	$deviceToken = User::leftjoin("device_token","users.id","=","device_token.user_id")->where("users.id",$coupon["user_id"])->get(['device_token.token','users.id']);


        if(count($deviceToken) > 0){
        foreach ($deviceToken as $value) {
            $token[] = $value->token;
            $activity = new Activity();
            $activity->user_id = $value->id;
            $activity->action  = 'view';
            $activity->object  = "coupon";
            $activity->action_id = $coupon["id"];
            $activity->object_id = $coupon["id"];
            $activity->actor_id = $value->id;
            $activity->save();
        }
        
        if(count($token) > 0){
            //$downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
            //$success = $downstreamResponse->numberSuccess();
        }
            
            
        }
        if($success){
           return true;
        }else{
           return false;
        }
    }

public function queue($queue, $command, $data) {
       $queueName = 'tree_coupon_expiry_notification_'.config('app.queue_post_fix');
       $queue->pushOn($queueName, $command, $data);
   }
}
