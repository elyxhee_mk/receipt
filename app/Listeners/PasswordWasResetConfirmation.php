<?php
namespace receipt\Listeners;

use receipt\Events\PasswordWasReset;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;


class PasswordWasResetConfirmation
{
    public $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  PasswordWasRecovered  $event
     * @return void
     */
    public function handle(PasswordWasReset $event)
    {
        $user = $event->user;
        $this->mailer->send('emails.user.reset-password', ['user' => $user], function ($m) use ($user) {
            $m->to($user->email)->subject('Reset Password');
        });
    }
}
