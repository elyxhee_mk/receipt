<?php

namespace receipt\Data\Repositories;

use Illuminate\Support\Facades\Event;
use receipt\Data\Models\UserFolder;
use receipt\Data\Models\User;
use receipt\Data\Models\UserReceipt;
use receipt\Data\Repositories\ReceiptRepository;
use receipt\Data\Contracts\RepositoryContract;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Hash, Illuminate\Support\Str;
use receipt\Support\Helper;
use \StdClass;

class FolderRepository extends AbstractRepository implements RepositoryContract {

    /**
     *
     * These will hold the instance of Folder Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;
    

    /**
     *
     * This is the prefix of the cache key to which the 
     * product data will be stored
     * product Auto incremented Id will be append to it
     *
     * Example: folder-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'folder-';
    protected $_cacheTotalKey = 'total-folder';
   

    public function __construct(UserFolder $folder,UserReceipt $receipt,ReceiptRepository $receiptRepo,User $user) {
        $this->builder = $folder;
        $this->model = $folder;
        $this->receipt = $receipt;
        $this->receiptRepo = $receiptRepo;
        $this->user = $user;
        
    }
    public function create(array $data = [],$details = false,$encode=true) {
        parent::setEncodedKeys(array('user_id'));
        if ($folder = parent::create($data,true,true)) {
            return $folder;
        }
        return false;
    }
    public function findById($id, $refresh = false, $details = false, $encode = true) {
        
        parent::setEncodedKeys(array('user_id'));
        $data = parent::findById($id, $refresh, $details, $encode);
        
        $folder_package = $this->userPackage(hashid_decode($data->user_id));      

        $folderReceipt = $this->model->find($id)->receipts;        
        $data->createReceipt = false;
        if(count($folderReceipt) < ($folder_package["receipt_per_folder"]))
        {            

            $data->createReceipt = true;
        }
 
        if(count($folderReceipt) > 0 )
        {
            
            $receipts = array();
            foreach($folderReceipt as $k=>$rec)
            {
                
                $receipt= new stdClass();
                $receipt->id = hashid_encode($rec->id);

                $receipt->doc_type = $rec->doc_type;
                $receipt->doc_name = $rec->doc_name;
                $receipt->receipt_date = $rec->receipt_date;
                $receipt->expiry_date = $rec->expiry_date;
                $receipt->notes = $rec->notes;

                if(isset($rec->path) && $rec->path != Null){

                    
                    if (strpos($rec->path, '.') !== false) {
                        list($file, $extension) = explode('.', $rec->path);
                        $file = config('app.files.receipt.folder_name')."/".$file;
                                        
                        $receipt->image_urls['1x'] = config('app.url').'/storage/app/'.$file.'.'.$extension;
                        $receipt->image_urls['2x'] = config('app.url').'/storage/app/'.$file.'@2x.'.$extension;
                        $receipt->image_urls['3x'] = config('app.url').'/storage/app/'.$file.'@3x.'.$extension;
                    } 
                }
                $receipts[] = $receipt;
            }
            $data->receipts = $receipts;
        }

        return $data;
    }

    public function hasRole($id, $role) {
        return $this->model_user_role->where('user_id', '=', $id)
                                ->where('role_id', '=', $role)
                                ->count() > 0;
    }
    public function userPackage($id) {
        
        $user_package = $this->user->find($id)->subscription;
        $package = config('app.packages.'.$user_package);
        return $package;
       
    }
    public function findByAll($pagination = false,$perPage = 10, $data = [], $detail = false, $encode = true){
       $folder = $this->builder;       
       
        
        if( isset($data['user_id']) && $data['user_id'] != ""){
            $folder = $folder->where('user_id',$data['user_id']); 
        } 
         
        $this->builder = $folder;
        //dd($this->builder->toSql(),$this->builder->getBindings());
        parent::setEncodedKeys(array('user_id'));
       $folder = parent::findByAll($pagination,$perPage,[],$detail,$encode);
       $folder_package = $this->userPackage($data['user_id']); 
        $folder['createFolder'] = false;
        if(count($folder['data']) < ($folder_package["allowed_folder"]))
        {            

            $folder['createFolder'] = true;
        }
     
        if($folder != NULL){
            return $folder;
        }else{
            return NULL;
        }
    }     
    public function update(array $data = [],$details = false,$encode=true) {
        
        parent::setEncodedKeys(array('user_id'));
        if ($folder = parent::update($data,true,false)) {
            return $folder;
        }
        return false;
    }     
    public function delete($id) {       
        
       
        if($this->receipt->where("folder_id",$id)->count() >0)
            $this->receipt->where("folder_id",$id)->delete();
        
        return $this->deleteById($id);
    }     
}