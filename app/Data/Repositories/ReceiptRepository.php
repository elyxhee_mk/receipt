<?php

namespace receipt\Data\Repositories;

use Illuminate\Support\Facades\Event;
use receipt\Data\Models\UserReceipt;
use receipt\Data\Models\UserFolder;
use receipt\Data\Contracts\RepositoryContract;
//use receipt\Data\Models\Content;


use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Hash, Illuminate\Support\Str;
use receipt\Support\Helper;
use Storage, Image,FFMpeg;
use \DB;

class ReceiptRepository extends AbstractRepository implements RepositoryContract {

    /**
     *
     * These will hold the instance of Content Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;
    

    /**
     *
     * This is the prefix of the cache key to which the 
     * product data will be stored
     * product Auto incremented Id will be append to it
     *
     * Example: product-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'content-';
    protected $_cacheTotalKey = 'total-content';
   

    public function __construct(UserReceipt $receipt) {
        $this->builder = $receipt;
        $this->model = $receipt;
        
    }
    public function create(array $data = [],$details = false,$encode=true) {
        if(isset($data['content']) && $data['content'] != NULL){
        $content = $this->saveImage($data['content']);
        if($content){
            // dd($content);
            $data['path'] = $content;            
           
        }
        }else{
            $data['path'] = "NULL";
            $data['original_name'] = "NULL";
            $data['type'] = "text";
            $data['file_type'] = "NULL";
        }
            
        unset($data['content']);
        unset($data['type']);        
        parent::setEncodedKeys(array('user_id','folder_id'));
        if ($receipt = parent::create($data,true,true)) {
            return $receipt;
        }
        return false;
    }
    public function findById($id, $refresh = false, $details = false, $encode = true,$postView = true,$recoverPassword=false) {
        $data = parent::findById($id, $refresh, $details, $encode);
        //dd($data,$details);
        if ($data) {           
            if($encode){
                $user_id = hashid_encode($data->id);
            }else{
                $user_id = ($data->id);
            }
            if(isset($data->path) && $data->path != Null){

                if (strpos($data->path, '.') !== false) {
                    list($file, $extension) = explode('.', $data->path);
                    $file = config('app.files.receipt.folder_name')."/".$file; 
                    //dd("files",$file,config('app.url')) ;                  
                    $data->image_urls['1x'] = config('app.url').'/storage/app/'.$file.'.'.$extension;
                    $data->image_urls['2x'] = config('app.url').'/storage/app/'.$file.'@2x.'.$extension;
                    $data->image_urls['3x'] = config('app.url').'/storage/app/'.$file.'@3x.'.$extension;
                } 
            }
           

        }        
        return $data;
    }

    public function hasRole($id, $role) {
        return $this->model_user_role->where('user_id', '=', $id)
                                ->where('role_id', '=', $role)
                                ->count() > 0;
    }

    private function crop($file, $extensions = ['jpeg', 'png', 'gif', 'bmp', 'svg']) {

        list($name, $ext) = explode('.', $file->hashName());
        if (in_array($file->guessExtension(), $extensions)) {
            $image = Image::make($file);
            $width = $image->width();
            $height = $image->height();
            $store = Storage::put(config('app.files.receipt.folder_name').'/'.$name.'@3x.'.$ext, $image->stream());
            $store = Storage::put(config('app.files.receipt.folder_name').'/'.$name.'@2x.'.$ext, $image->resize($width / 1.5, $height / 1.5)->stream());
            $store = Storage::put(config('app.files.receipt.folder_name').'/'.$name.'.'.$ext, $image->resize($width / 3, $height / 3)->stream());
        }
        return true;
    }
    /*will be needing this when user will be searching for store with its description*/
    public function storeSearch($pagination = false,$perPage = 10, $data = []){

        $this->builder = $this->model
        ->where(function($query) use($data) {
            $query->where('username', 'LIKE', "%{$data['keyword']}%");
            $query->orWhere('fullname', 'LIKE', "%{$data['keyword']}%");
        })
        ->where('visibility','=',1)
        ->leftJoin('user_roles',function($joins){
            $joins->on('users.id','=','user_roles.user_id');
        })->where('user_roles.role_id','=', Role::USER)
        ->select('users.id');

        return parent::findByAll($pagination, $perPage);
    }

    public function findByAll($pagination = false,$perPage = 10, $data = [], $detail = false, $encode = true){

        //dd("find by all",$data);
       $content = $this->builder; 
       if( isset($data['user_id']) && $data['user_id'] != ""){
            $content = $content->join('user_folders','user_receipts.folder_id',"=",'user_folders.id')->where('user_id',$data['user_id']); 
        }       
       
        if( isset($data['id']) && $data['id'] != ""){
            $content = $content->where('id',$data['id']); 
        }
         if( isset($data['folder_id']) && $data['folder_id'] != ""){
            $content = $content->where('folder_id',$data['folder_id']); 
        }        
         if( isset($data['doc_type']) && $data['doc_type'] != ""){
            $content = $content->where('doc_type',$data['doc_type']); 
        } 
        $this->builder = $content; 
        $this->builder->select('user_receipts.*','user_folders.id as folder_id');
        //dd($this->builder->toSql(),$this->builder->getBindings());      
       $receipt = parent::findByAll($pagination,$perPage,[],$detail,$encode);      
        if($receipt != NULL){
            return $receipt;
        }else{
            return NULL;
        }
    } 
    private function saveImage($image) {
        //dd(config('app.files.receipt.folder_name'));
        $action = $image->store(config('app.files.receipt.folder_name'));
        if($action){
            $crop = $this->crop($image);
        }
        return $image->hashName(); //config('app.files.receipt.folder_name').'/'.$name.'.'.$ext;
    } 
    public function update(array $data = [],$details = false,$encode=true) {

        parent::setEncodedKeys(array('user_id','folder_id'));
        if ($course = parent::update($data,true,false)) {
            return $course;
        }
        return false;
    }       

}