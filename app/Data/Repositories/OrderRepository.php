<?php

namespace receipt\Data\Repositories;

use Illuminate\Support\Facades\Event;
use receipt\Data\Contracts\RepositoryContract;
use receipt\User;
use receipt\Data\Models\Order;
use Illuminate\Support\Facades\Cache;
use JWTAuth, Carbon\Carbon;
use Hash, Illuminate\Support\Str;
use receipt\Support\Helper;
use Storage, Image;
use \App;
use Tymon\JWTAuth\Payload;
use Validator;


class OrderRepository extends AbstractRepository implements RepositoryContract {

    /**
     *
     * These will hold the instance of User Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;
    public $model_product;

    /**
     *
     * This is the prefix of the cache key to which the 
     * order data will be stored
     * order Auto incremented Id will be append to it
     *
     * Example: order-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'order-';
    protected $_cacheTotalKey = 'total-order';
   

    public function __construct(Order $order) {
        $this->builder = $order;
        $this->model = $order;        
    }
    public function create(array $data = [],$details = false,$encode=true) {

        parent::setEncodedKeys(array("customer_id"));
        return parent::create($data,true,true);
        
    }
    public function findById($id, $refresh = false, $details = false, $encode = true) {
        $data = parent::findById($id, $refresh, $details, $encode);

        if ($data) {            
            if($encode){
                $user_id = hashid_encode($data->id);
            }else{
                $user_id = ($data->id);
            }           

        }

        return $data;
    }
    public function findByAll($pagination = false,$perPage = 10, $data = [], $detail = false, $encode = true){
        //dd($data);
       $order = $this->builder;       
       if( isset($data['id']) && $data['id'] != ""){
            $order = $order->where('id',$data['id']); 
        }
        if( isset($data['filter_user_id']) && $data['filter_user_id'] != ""){
            $order = $order->where('user_id',$data['filter_user_id']); 
        }
        /*if( isset($data['user_id']) && $data['user_id'] != ""){
            $order = $order->where('orders.customer_id',$data['user_id']); 
        }*/// 
        if(isset($data['order_id']) && $data['order_id'] != ""){
            //dd("having order id");
            $order = $order->where('orders.id',$data['order_id']); 
        }

        if( isset($data['customer_id']) && $data['customer_id'] != ""){
            $order = $order->where('customer_id',$data['customer_id']); 
        } 
        // in case of store_id need to add join with store_products
         if( isset($data['store_id']) && $data['store_id'] != ""){            
            $order = $order->join("order_products",'orders.id','=','order_products.order_id')->where('store_id',$data['store_id']); 
        }
        /*if( isset($data['details']) && $data['details'] == "true"){            
            $order = $order->leftjoin("order_products",'orders.id','=','order_products.order_id');
        }*/

        if( isset($data['order_date']) && $data['order_date'] != ""){
            $order = $order->where('created_at',$data['order_date']); 
        }
        if( isset($data['paid_date']) && $data['paid_date'] != ""){
            $order = $order->where('paid_date',$data['paid_date']); 
        }
         if( isset($data['paid']) && $data['paid'] != ""){
            $paid = ($data['paid'] == "true") ? 1 : 0;
            $order = $order->where('paid',$paid); 
        }          
      
        $order = $order->orderBy("orders.created_at","desc");
        $this->builder = $order;
        $orders = parent::findByAll($pagination,$perPage,[],$detail,$encode);
        
        if($orders != NULL){
            return $orders;
        }else{
            return NULL;
        }
    }
    public function update(array $data = [],$encode = true) {

       if($this->_cacheKey.$data['id'] !== NULL)
        Cache::forget($this->_cacheKey.$data['id']);
        
       parent::setEncodedKeys(array("customer_id"));
        if ($order = parent::update($data,"","",$encode)) {           
            return $order;
        }
        return false;
    }
    
              

}