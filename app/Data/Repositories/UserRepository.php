<?php

namespace receipt\Data\Repositories;

use Illuminate\Support\Facades\Event;
use receipt\Data\Contracts\RepositoryContract;
use receipt\User;
use receipt\Data\Models\UserFriend;
use receipt\Data\Models\UserProfileAction;
use receipt\Data\Models\UserSocialAccount;
use Illuminate\Support\Facades\Cache;

use JWTAuth, Carbon\Carbon;
use Hash, Illuminate\Support\Str;
//use receipt\Support\Helper;
use receipt\Support\Helper;
use Storage, Image;
use \App;
use Tymon\JWTAuth\Payload;
use Validator;


class UserRepository extends AbstractRepository implements RepositoryContract {

    /**
     *
     * These will hold the instance of User Class.
     *
     * @var object
     * @access public
     *
     **/
    public $model;
    public $model_business_location;

    /**
     *
     * This is the prefix of the cache key to which the 
     * user data will be stored
     * user Auto incremented Id will be append to it
     *
     * Example: user-1
     *
     * @var string
     * @access protected
     *
     **/
    protected $_cacheKey = 'user-';
    protected $_cacheTotalKey = 'total-users';
    protected $_cacheRolesKey = 'user-role';

    public function __construct(User $user,UserSocialAccount $userSocialAccount) {
        $this->builder = $user;
        $this->model = $user;
       /* $this->model_business_location = $business_location;*/
        $this->model_user_social_account = $userSocialAccount;
    }
	
	public function findById($id, $refresh = false, $details = false, $encode = true,$postView = true,$recoverPassword=false) {
        $data = parent::findById($id, $refresh, $details, $encode);

        if ($data) {
            if (!$details) {
                unset($data->password);
                unset($data->deleted_at);
                unset($data->access_token);
            }
            $data->profile_view_count = UserProfileAction::where("user_id","=",hashid_decode($data->id))->count();
                    
            try{
                $token = JWTAuth::getToken();
                if($token){
                    $tokenUser = JWTAuth::toUser($token);                    
                }
                
            }catch(Exception $e){

            }
            if($encode){               
                $user_id = hashid_encode($data->id);
            }else{
                $user_id = ($data->id);
            }
            if($data->created_at){
                $data->join_date = $data->created_at;
            }

            $socialAccountConnected = $this->model_user_social_account->where('user_id','=', hashid_decode($data->id))->first();           
            if(isset($data->profile_pic) && $data->profile_pic != Null){

                if (strpos($data->profile_pic, '.') !== false) {
                    list($file, $extension) = explode('.', $data->profile_pic);
                    $file = config('app.files.users.folder_name')."/".$file;
                    
                    $data->image_urls['1x'] = config('app.url').'storage/app/'.$file.'.'.$extension;
                    $data->image_urls['2x'] = config('app.url').'storage/app/'.$file.'@2x.'.$extension;
                    $data->image_urls['3x'] = config('app.url').'storage/app/'.$file.'@3x.'.$extension;
                } else {
                    
                    if(($data->signup_via == 'facebook' || $data->signup_via == 'twitter' || $data->signup_via == 'google') && $socialAccountConnected != Null){
                        /*$data->image_urls['1x'] = 'http://graph.facebook.com/'.$socialAccountConnected->social_network_id.'/picture?width=180&height=180';
                        $data->image_urls['2x'] = 'http://graph.facebook.com/'.$socialAccountConnected->social_network_id.'/picture?width=180&height=180';
                        $data->image_urls['3x'] = 'http://graph.facebook.com/'.$socialAccountConnected->social_network_id.'/picture?width=360&height=360';*/
                        $data->image_urls['1x'] = $data->profile_pic;
                        $data->image_urls['2x'] = $data->profile_pic;
                        $data->image_urls['3x'] = $data->profile_pic;
                    }
                }   
            }
		else{
                        $data->image_urls = new \stdClass;
                    }


            if(isset($data->dream_collage) && $data->dream_collage != Null){

                if (strpos($data->dream_collage, '.') !== false) {
                    list($file, $extension) = explode('.', $data->dream_collage);
                    $file = config('app.files.collage.folder_name')."/".$file;
                    // dd($file);
                    $data->dream_collage_url['1x'] = config('app.url').'storage/app/public/files/'.$file.'.'.$extension;
                } else {
                    
                        $data->dream_collage_url = new \stdClass;
                }   
            }
            
        }

        return $data;
    }

    public function register(array $data = [], $role_id = 0){
        if(isset($data['password']) && !empty($data['password'])){
             $data['password'] = Hash::make($data['password']);
             
        }else{
            $data['password'] = Hash::make(rand(7,5));            
        }
        if($data['signup_via'] == 'email' && isset($data['file'])){
            $saveImage = $this->crop($data['file']);
            if($saveImage){
                unset($data['file']);
            }    
        }
        elseif($data['signup_via'] != 'email' && isset($data['file']))
        {
           
	$data['profile_pic'] = $data['file'];
        unset($data['file']);
		
        }
        

        $deviceData=[];
        if(isset($data['device_token']) && isset($data['udid']) && isset($data['device_type'])){
                $deviceData['token'] = $data['device_token'];
                $deviceData['udid'] = $data['udid'];
                $deviceData['type'] = $data['device_type'];
                $deviceRepo = App::make('DeviceTokenRepository');
                unset($data['device_token'],$data['udid'],$data['device_type']);
        }
        

        if(isset($data["social_network_id"]) && $data["social_network_id"] != ""){
            $socialNetworkId = $data["social_network_id"];
        }
        
        
        unset($data["social_network_id"]);        
            if ($user = parent::create($data,true,false)) {
                if(isset($location)){
                    // dd($location);
                foreach ($location as $key => $value) {
                    $business_location_data = new BusinessLocation();
                    $business_location_data->name = $value->name;
                    $business_location_data->longitude = $value->longitude;
                    $business_location_data->lattitude = $value->lattitude;
                    $business_location_data->user_id = ($user->id);
                    $business_location_data->created_at = Carbon::now();
                    $business_location_data->save();
                }    
            }
                
                if( isset($socialNetworkId) && $socialNetworkId != "" ){
                    $socialAccountInsatance = $this->model_user_social_account->newInstance();
                    $socialAccountInsatance->user_id = $user->id;
                    $socialAccountInsatance->social_network_id = $socialNetworkId;
                    $socialAccountInsatance->social_network = $data["signup_via"];
                    if($socialAccountInsatance->save()){

                        if(count($deviceData) > 0 && is_array($deviceData)){
                                $deviceData['user_id'] = $user->id;
                                $setDeviceToken = $deviceRepo->setUserTokens($deviceData);
                        }
                        return $this->update([
                            'id' => $user->id,
                            'login_at' => Carbon::now(),
                            'access_token' => JWTAuth::fromUser($user),
                        ],true,true);
                    }
                } else {
                    if(count($deviceData) > 0 && is_array($deviceData)){
                                $deviceData['user_id'] = $user->id;
                                $setDeviceToken = $deviceRepo->setUserTokens($deviceData);
                    }
                    
                }
                    return $this->update([
                            'id' => $user->id,
                            'login_at' => Carbon::now(),
                            'access_token' => JWTAuth::fromUser($user),
                    ],true,true);
                // }
            }
        return false;
    }


    public function findById_backup($id, $refresh = false, $details = false, $encode = true,$postView = true,$recoverPassword=false) {
        $data = parent::findById($id, $refresh, $details, $encode);

        if ($data) {
            if (!$details) {
                unset($data->password);
                unset($data->deleted_at);
                unset($data->access_token);
            }
            $data->profile_view_count = UserProfileAction::where("user_id","=",hashid_decode($data->id))->count();
                    
            try{
                $token = JWTAuth::getToken();
                if($token){
                    $tokenUser = JWTAuth::toUser($token);
                    /*$data->mutual_friends_count = $this->countMutualFriends($tokenUser->id,hashid_decode($data->id)); 
                    $isFriendWithYou = UserFriend::whereRaw("((user_id = ".hashid_decode($data->id)." and friend_id = ".$tokenUser->id.") or (user_id = ".$tokenUser->id." and friend_id = ".hashid_decode($data->id)."))")->first();
                    if($isFriendWithYou){
                        $data->is_friend = $isFriendWithYou->status;
                        $isFriendWithYou->id = hashid_encode($isFriendWithYou->id);
                        $isFriendWithYou->user_id = hashid_encode($isFriendWithYou->user_id);
                        $isFriendWithYou->friend_id = hashid_encode($isFriendWithYou->friend_id);
                        $isFriendWithYou->blocked_id = hashid_encode($isFriendWithYou->blocked_id);
                        $data->is_friend_data = $isFriendWithYou;
                    }*/
                }
                
            }catch(Exception $e){

            }

            /*if($data->user_type == "business"){
                // echo $data->id;
                $businessLocation = BusinessLocation::where("user_id","=",hashid_decode($data->id))->get(); 
                $finalBusinesslocation = [];
                if(count($businessLocation) > 0){
                    foreach ($businessLocation as $key => $value) {
                        $finalBusinesslocation[$key] = new \stdClass;
                        $finalBusinesslocation[$key]->id = hashid_encode($value->id);
                        $finalBusinesslocation[$key]->user_id = hashid_encode($value->user_id);
                        $finalBusinesslocation[$key]->longitude = $value->longitude;
                        $finalBusinesslocation[$key]->lattitude = $value->lattitude;
                        $finalBusinesslocation[$key]->name = $value->name;
                        $finalBusinesslocation[$key]->created_at = $value->created_at;
                        $finalBusinesslocation[$key]->updated_at = $value->updated_at;
                        $finalBusinesslocation[$key]->deleted_at = $value->deleted_at;
                    }
                }
                $data->business_location = $finalBusinesslocation;   
            }*/
            // dd($data);
            

            if($encode){
                $user_id = hashid_encode($data->id);
            }else{
                $user_id = ($data->id);
            }
            
           

            if($data->created_at){
                $data->join_date = $data->created_at;
            }

            // $socialAccountConnected = $this->model_user_social_account->where('user_id','=', $user_id)->first();
            if(isset($data->profile_pic) && $data->profile_pic != Null){

                if (strpos($data->profile_pic, '.') !== false) {
                    list($file, $extension) = explode('.', $data->profile_pic);
                    $file = config('app.files.users.folder_name')."/".$file;
                    // dd($file);
                    $data->image_urls['1x'] = config('app.url').'storage/app/'.$file.'.'.$extension;
                    $data->image_urls['2x'] = config('app.url').'storage/app/'.$file.'@2x.'.$extension;
                    $data->image_urls['3x'] = config('app.url').'storage/app/'.$file.'@3x.'.$extension;
                } else {
                    if($data->signup_via == 'facebook' && $socialAccountConnected != Null){
                        $data->image_urls['1x'] = 'http://graph.facebook.com/'.$socialAccountConnected->social_network_id.'/picture?width=180&height=180';
                        $data->image_urls['2x'] = 'http://graph.facebook.com/'.$socialAccountConnected->social_network_id.'/picture?width=180&height=180';
                        $data->image_urls['3x'] = 'http://graph.facebook.com/'.$socialAccountConnected->social_network_id.'/picture?width=360&height=360';
                    }else{
                        $data->image_urls = new \stdClass;
                    }
                }   
            }


            if(isset($data->dream_collage) && $data->dream_collage != Null){

                if (strpos($data->dream_collage, '.') !== false) {
                    list($file, $extension) = explode('.', $data->dream_collage);
                    $file = config('app.files.collage.folder_name')."/".$file;
                    // dd($file);
                    $data->dream_collage_url['1x'] = config('app.url').'storage/app/public/files/'.$file.'.'.$extension;
                } else {
                    
                        $data->dream_collage_url = new \stdClass;
                }   
            }
            
            
            
            
            /*if( $data->image_path != null ){
                $data->profile_pic = config('app.files.users.full_path')."/".$data->profile_pic;
            }*/
            
        }

        return $data;
    }

    public function register_bkup(array $data = [], $role_id = 0){
        if(isset($data['password']) && !empty($data['password'])){
             $data['password'] = Hash::make($data['password']);
             // $data['create_password'] =  1 ;
        }else{
            $data['password'] = Hash::make(rand(7,5));
            // $data['create_password'] =  0 ;
        }
        if($data['signup_via'] == 'email' && isset($data['file'])){
            $saveImage = $this->crop($data['file']);
            if($saveImage){
                unset($data['file']);
            }    
        }
        

        $deviceData=[];
        if(isset($data['device_token']) && isset($data['udid']) && isset($data['device_type'])){
                $deviceData['token'] = $data['device_token'];
                $deviceData['udid'] = $data['udid'];
                $deviceData['type'] = $data['device_type'];
                $deviceRepo = App::make('DeviceTokenRepository');
                unset($data['device_token'],$data['udid'],$data['device_type']);
        }
        

        if(isset($data["social_network_id"]) && $data["social_network_id"] != ""){
            $socialNetworkId = $data["social_network_id"];
        }
        
        // unset($data["fb_access_token"]);
        unset($data["social_network_id"]);
        // $userDataRole = $this->model->join('user_roles', 'users.id', '=', 'user_roles.user_id')->where('users.email', '=', $data['email'])->first(['user_roles.role_id', 'user_roles.user_id']);
        
        // if( $userDataRole == NULL || $data['signup_via'] == "facebook"){
        if(isset($data['user_type']) && $data['user_type'] == 'business'){
                    $location = $data['business_location'];
                    unset($data['business_location']);
        }
            if ($user = parent::create($data,true,false)) {
                if(isset($location)){
                    // dd($location);
                foreach ($location as $key => $value) {
                    $business_location_data = new BusinessLocation();
                    $business_location_data->name = $value->name;
                    $business_location_data->longitude = $value->longitude;
                    $business_location_data->lattitude = $value->lattitude;
                    $business_location_data->user_id = ($user->id);
                    $business_location_data->created_at = Carbon::now();
                    $business_location_data->save();
                }    
            }
                
                // $business_location_data = $this->model_business_location->insert($location);
                // Cache::forget($this->_cacheRolesKey.$user->id);

                if( isset($socialNetworkId) && $socialNetworkId != "" ){
                    $socialAccountInsatance = $this->model_user_social_account->newInstance();
                    $socialAccountInsatance->user_id = $user->id;
                    $socialAccountInsatance->social_network_id = $socialNetworkId;
                    $socialAccountInsatance->social_network = "facebook";
                    if($socialAccountInsatance->save()){

                        if(count($deviceData) > 0 && is_array($deviceData)){
                                $deviceData['user_id'] = $user->id;
                                $setDeviceToken = $deviceRepo->setUserTokens($deviceData);
                        }
                        return $this->update([
                            'id' => $user->id,
                            'login_at' => Carbon::now(),
                            'access_token' => JWTAuth::fromUser($user),
                        ],true,true);
                    }
                } else {
                    if(count($deviceData) > 0 && is_array($deviceData)){
                                $deviceData['user_id'] = $user->id;
                                $setDeviceToken = $deviceRepo->setUserTokens($deviceData);
                    }
                    
                }
                    return $this->update([
                            'id' => $user->id,
                            'login_at' => Carbon::now(),
                            'access_token' => JWTAuth::fromUser($user),
                    ],true,true);
                // }
            }
        // } else {

        //     if( $userDataRole->role_id == 1){
        //         $userRole = new UserRole();
        //         $userRole->user_id = $userDataRole->user_id;
        //         $userRole->role_id = Role::USER;
        //     }else{
        //         $userRole = new UserRole();
        //         $userRole->user_id = $userDataRole->user_id;
        //         $userRole->role_id = Role::ADMINISTRATOR;
        //     }
        //     Cache::forget($this->_cacheRolesKey.$userDataRole->user_id);
        //     if( $userRole->save() ){

        //         if($role_id != Role::ADMINISTRATOR){
        //             $genres = Genre::orderby("order","asc")->limit(5)->get(['id']);
        //             $userFavouriteGenre['genre_id']=[];
        //             foreach ($genres as $genreid) {
        //                 $userFavouriteGenre['genre_id'][] = $genreid->id;
        //             }
        //             if(count($userFavouriteGenre['genre_id']) > 0){
        //                 $userFavouriteGenre['user_id'] = $user->id;
        //                 $userFavGenre = $this->userGernreRepo->setFavourite($userFavouriteGenre);
        //             }
                        
        //         }

        //         return $this->update([
        //                         'id' => $userDataRole->user_id,
        //                         'login_at' => Carbon::now(),
        //                         'access_token' => JWTAuth::fromUser($userDataRole),
        //                     ],true,true);
        //     }
            
        // }
        return false;
    }

    public function login($data) {

        $user = $this->findByAttribute('email', $data['email'], false, true, false);        
        if ($user != NULL ) {
            // if($user->visibility == 1){

                if (Hash::check($data['password'], $user->password)) {
                    
                    if(isset($data['device_token']) && isset($data['udid']) && isset($data['device_type'])){
                            $deviceData['user_id'] = $user->id;
                            $deviceData['token'] = $data['device_token'];
                            $deviceData['udid'] = $data['udid'];
                            $deviceData['type'] = $data['device_type'];
                            $deviceRepo = App::make('DeviceTokenRepository');
                            $setDeviceToken = $deviceRepo->setUserTokens($deviceData);
                    }
                    return $this->update([
                        'id' => $user->id,
                        'login_at' => Carbon::now(),
                        'access_token' => JWTAuth::fromUser($user),
                    ],true,true);

                }
            // }else{
            //     return 'user_block';
            // }
        }
        return false;
    }

    public function hasRole($id, $role) {
        return $this->model_user_role->where('user_id', '=', $id)
                                ->where('role_id', '=', $role)
                                ->count() > 0;
    }

    private function crop($file, $extensions = ['jpeg', 'png', 'gif', 'bmp', 'svg']) {

        list($name, $ext) = explode('.', $file->hashName());
        if (in_array($file->guessExtension(), $extensions)) {
            $image = Image::make($file);
            $width = $image->width();
            $height = $image->height();
            $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'@3x.'.$ext, $image->stream());
            $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'@2x.'.$ext, $image->resize($width / 1.5, $height / 1.5)->stream());
            $store = Storage::put(config('app.files.users.folder_name').'/'.$name.'.'.$ext, $image->resize($width / 3, $height / 3)->stream());
        }
        return true;
    }

    public function userSearch($pagination = false,$perPage = 10, $data = []){

        $this->builder = $this->model
        ->where(function($query) use($data) {
            $query->where('username', 'LIKE', "%{$data['keyword']}%");
            $query->orWhere('fullname', 'LIKE', "%{$data['keyword']}%");
        })
        ->where('visibility','=',1)
        ->leftJoin('user_roles',function($joins){
            $joins->on('users.id','=','user_roles.user_id');
        })->where('user_roles.role_id','=', Role::USER)
        ->select('users.id');

        return parent::findByAll($pagination, $perPage);
    }

    public function findByAll($pagination = false,$perPage = 10, $data = [], $detail = false, $encode = true, $postView = true){
       $users = $this->builder;
       //dd($data);
        // if(isset($data['role_id']) && $data['role_id'] == Role::ADMINISTRATOR){
        //     $users = $this->model->join('user_roles','user_roles.user_id','=','users.id')->where('user_roles.role_id','=',Role::ADMINISTRATOR)->select(['users.id as id']);
        // }else{
        //     $users = $this->model->join('user_roles','user_roles.user_id','=','users.id')->where('user_roles.role_id','=',Role::USER)->where('users.id','!=',$data['user_id'])->select(['users.id as id']);
        // }
        if( isset($data['filter_by_name']) && $data['filter_by_name'] != ""){
            $users = $users->whereRaw('(users.username LIKE "%'.$data['filter_by_name'].'%" OR users.username LIKE "%'.$data['filter_by_name'].'%")');
        }
        if( isset($data['filter_by_type']) && $data['filter_by_type'] != ""){
            $users = $users->where('users.user_type', '=', $data['filter_by_type']);
        }
        if( isset($data['filter_by_gender']) && $data['filter_by_gender'] != ""){
            $users = $users->where('users.gender', '=', $data['filter_by_gender']);
        }        
        
        $this->builder = $users;
        $order = $this->builder->orderBy("users.created_at","desc");
        $this->builder = $order;
        //dd($this->builder->toSql());
        $user = parent::findByAll($pagination,$perPage,[],$detail,$encode,$postView);
        //dd($user);
        if($user != NULL){
            return $user;
        }else{
            return NULL;
        }
    }

    
   
    
    
    
    // public function setUserVisibility(array $data = []){
    //     $user = User::find($data['id']);

    //     $user->visibility = $data['is_visible'];

    //     if($user->save()){
    //         if($data['is_visible'] == 0){
                
    //             \DB::table('activities')
    //                 ->where(function($query) use ($data){
    //                     $query->where('activities.actor_id', '=', $data['id'])
    //                     ->orWhere('activities.user_id', '=', $data['id']);
    //                 })
    //                 ->update(['visibility' => 0]);
    //             }

    //         Cache::forget($this->_cacheKey.$data['id']);
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    // public function connectSocialAccount(array $data){
       
        
    //     $socialNetworkExists = $this->model_user_social_account->where('user_id','=',$data['user_id'])
    //                                     ->where('social_network','=', $data['social_network'])
    //                                     ->where('social_network_id','=', $data['social_network_id'])
    //                                     ->first();
    //     if ($socialNetworkExists == NULL) {
    //         $isAlreadyConnected = $this->model_user_social_account->where('user_id','=',$data['user_id'])->count();
    //         if ($isAlreadyConnected) {
    //             return 'already_connected';
    //         }            
    //         $socialNetwork = $this->model_user_social_account->newInstance();
    //         $socialNetwork->user_id = $data['user_id'];
    //         $socialNetwork->social_network_id = $data['social_network_id'];
    //         $socialNetwork->social_network = $data['social_network'];
    //         if ($socialNetwork->save()) {
    //                  return $this->update([
    //                     'id' => $socialNetwork->user_id,
    //                     'updated_at' => Carbon::now(),
    //                 ],true,true);
    //         } else {
    //             return false;
    //         }
    //     } else {
    //         return 'current_already_connected';
    //     }
    // }

    // public function disconnectSocialAccount(array $data){
    //     $isAlreadyConnected = $this->model_user_social_account->where('user_id','=',$data['user_id'])->count();
    //     if ($isAlreadyConnected < 1) {
    //         return 'not_connected';
    //     }
        
    //     $socialNetworkExists = $this->model_user_social_account->where('user_id','=',$data['user_id'])->delete();
    //      return $this->update([
    //                     'id' => $data['user_id'],
    //                     'updated_at' => Carbon::now(),
    //                     ],true,true);
    // }    
    public function createProfileView(array $data = []) {
        $response = UserProfileAction::create($data);
        return $response;
    }                  

}