<?php

namespace receipt\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TextContent extends Model
{
    use SoftDeletes;   
}
