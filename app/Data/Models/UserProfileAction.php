<?php

namespace receipt\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserProfileAction extends Model
{
	protected $table = 'user_profile_actions';
	protected $fillable = ['user_id', 'profile_id', 'type', 'created_at', 'updated_at'];
    use SoftDeletes;
    
}
