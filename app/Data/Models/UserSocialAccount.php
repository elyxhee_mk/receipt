<?php

namespace receipt\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSocialAccount extends Model
{
    use SoftDeletes;
}
