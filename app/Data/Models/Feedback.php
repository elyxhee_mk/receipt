<?php

namespace receipt\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feedback extends Model
{
    use SoftDeletes; 

    public function user() 
    {

    	return $this->belongsTo('receipt\Data\Models\User');
    }
}
