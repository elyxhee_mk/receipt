<?php

namespace receipt\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserFolder extends Model
{
    use SoftDeletes;
    protected $table = "user_folders";

    public function receipts()
    {
    	 return $this->hasMany('receipt\Data\Models\UserReceipt','folder_id');
    }

	protected static function boot() {
        parent::boot();

        static::deleting(function($userfolder) { // before delete() method call this
            $userfolder->receipts()->delete();
	            
        });
    }
}
