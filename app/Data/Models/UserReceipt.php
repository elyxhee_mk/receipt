<?php

namespace receipt\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserReceipt extends Model
{
    use SoftDeletes;
    protected $table = "user_receipts";
}
