<?php

namespace receipt\Support;
use Request;
use Illuminate\Http\Request as ServerRequest;
use DB;


class Helper {

	const VIDEO_CREATED = 1201;
	const VIDEO_COMMENT = 1202;
	const VIDEO_LIKED   = 1203;
	const PHOTO_CREATED = 1301;
	const PHOTO_COMMENT = 1302;
    const PHOTO_LIKED   = 1303;
	const FOLLOWER_REQUEST   = 1401;

	public static function appSettings() {
		$isSecure = config('app.is_secure');
		$appSettings = config('app.settings');
		$endPoints = $appSettings['auth']['endpoint'];
		$tokens = $appSettings['auth']['token'];
		unset($endPoints['token']);
		$selectiveSettings = [
				'baseUrl' => url('/'),
            	'apiUrl' => $endPoints['api'].'/api',
            	'auth' => [
            		'endpoint' => $endPoints,
            		'token'	=> $tokens
            	],
            	'program_start_date' => '2016-10-01',
                'facebook_app_id'=>config('app.facebook_app_id'),
        ];

		$settings = array_merge($selectiveSettings,$appSettings);
		return $settings;
	}

	

    public static function getFileUrl($url){
        $currentFileSystem = config('filesystems.default');
        if ($currentFileSystem == 'local' || $currentFileSystem == 'public') {
            return \URL::to('files/'.$url);
        } else if($currentFileSystem == 'rackspace'){
            if (config('app.is_secure')) {
                $storageUrl = config('filesystems.disks.rackspace.secure_url').$url;
            } else {
                $storageUrl = config('filesystems.disks.rackspace.public_url').$url;
            }
            return $storageUrl;
        } else {
            $storageUrl = \Storage::url($url);
            $storageUrl = str_replace('%40','@',$storageUrl);
            return $storageUrl;
        }
    }
}
