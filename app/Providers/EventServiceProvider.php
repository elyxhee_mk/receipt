<?php

namespace receipt\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'receipt\Events\Event' => [
            'receipt\Listeners\EventListener',
        ],
        'receipt\Events\test' => [
            'receipt\Listeners\testconfirm',
        ],
	'receipt\Events\CouponExpiryWasGenerated' => [
            'receipt\Listeners\CouponExpiryNotificationConfirmation',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
