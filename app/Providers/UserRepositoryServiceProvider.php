<?php

namespace receipt\Providers;

use Illuminate\Support\ServiceProvider;
use receipt\Data\Repositories\UserRepository;
use receipt\User;
use receipt\Data\Models\UserCode;
use receipt\Data\Models\Role;
use receipt\Data\Models\UserRole;
/*use konnect\Data\Models\PostAction;
use konnect\Data\Models\Post;
use konnect\Data\Models\UserSocialAccount;
use konnect\Data\Models\BusinessLocation;*/

class UserRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('UserRepository', function () {
            return new UserRepository(new User);
        });
    }
}
