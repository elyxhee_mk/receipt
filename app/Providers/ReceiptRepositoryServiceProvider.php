<?php

namespace receipt\Providers;

use Illuminate\Support\ServiceProvider;
use receipt\Data\Repositories\ReceiptRepository;
use receipt\Data\Models\UserReceipt;



class ReceiptRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ReceiptRepository', function () {
            return new ReceiptRepository(new UserReceipt);
        });
    }
}
