<?php

namespace receipt\Providers;

use Illuminate\Support\ServiceProvider;
use receipt\Data\Repositories\ContentRepository;
use receipt\Data\Models\Content;
use receipt\Data\Models\Role;
use receipt\Data\Models\UserRole;


class ContentRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ContentRepository', function () {
            return new ContentRepository(new Content);
        });
    }
}
