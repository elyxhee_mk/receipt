@extends('admin.layouts.master')

<!-- ############ Main START-->

@section('content')


<div>
    <div class="padding">
      <!-- <div>
        <button class="btn btn-success">Add player</button>
        <hr>
      </div> -->

      <div class="row">
            <div class="col-sm-6">
              <h4>User Records</h4>
            </div>

            <div class="col-sm-6">
              <div class="row">
                <div class="form-group col-sm-5">
                  <!-- <label for="inputState" class="d-block">Search by</label> -->
                  <select id="searchby" class="custom-select w-100">
                    <option selected="" value="name">Name</option>
                  </select>
                </div>
                <div class="form-group col-sm-5" id="Keyword">
                  <input type="text" class="form-control" id="SearchUser">
                </div>
                <div class="form-group col-sm-2">
                  <button class="btn btn-default" id="searchbtn"><i class="fa fa-search"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>

      <div class="table-responsive mt-2">
            <table id="" class="table v-middle p-0 m-0 box" data-plugin="dataTable">
              <thead>
                <tr>
                <th></th>
                  <!-- <th>Username</th> -->
                  <th>Name</th>
                  <th>Gender</th>
                  <th>Email</th>
                  <th>User type</th>
                  <th>Subscription type</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($data as $key => $value): ?>

                  <tr>
                    <td>
                    <?php if ($value->profile_pic != "" || $value->profile_pic != null): ?>
                      <span class="w-40 avatar circle">
                      <img src="{{$value->image_urls['1x']}}">
                  </span>
                    <?php endif ?>
                    <?php if ($value->profile_pic == ""): ?>
                      <span class="w-40 avatar circle">
                        <img src="{{asset('images/default.png')}}">
                    </span>
                    <?php endif ?>
                  
                </td>
                    <!-- <td>{{$value->username}}</td> -->
                    <td style="max-width: 100px;overflow: hidden;">{{ucwords($value->first_name.' '.$value->last_name)}}</td>
                    <td>{{$value->gender}}</td>
                    <td>{{$value->email}}</td>
                    <td>{{$value->user_type}}</td>
                    <td>{{$value->subscription}}</td>
                    
                  </tr>

                
              <?php endforeach ?>


              </tbody>
            </table>
            <br>  
            {{$data->links()}}
          </div>

    </div>
  </div>


<!-- ############ Main END-->

      </div>
      <!-- Footer -->
      <div class="content-footer white " id="content-footer">
        <div class="d-flex p-3">
          <span class="text-sm text-muted flex">&copy; Copyright. Flatfull</span>
          <div class="text-sm text-muted">Version 1.1.1</div>
        </div>
      </div>
  </div>
  <!-- ############ Content END-->

<!-- ############ LAYOUT END-->
</div>

<script>

  var getAppUrl = function(){
    return $('base').attr('href');
  };

  // User search
  $('#SearchUser').keyup(function(){
    var jsonData = {
      name:$(this).val(),
      }

    var request = $.ajax({
      url: getAppUrl()+'/users',
      data: jsonData,
      type: 'GET',
      dataType:'json'
    });
    request.done(function(data){
      if(data.response.code == 200) {
        
        $('tbody').empty();

        for (var i = 0; i < data.response.data.data.length; i++) {

          $('tbody').append("<tr><td></td>\
            <td>"+data.response.data.data[i].first_name+" "+data.response.data.data[i].last_name+"</td>\
            <td>"+data.response.data.data[i].gender+"</td>\
            <td>"+data.response.data.data[i].email+"</td>\
            <td></td>\
            </tr>"
          );
        }

      } 
    });
    request.fail(function(data){});
  });


</script>

@stop
<!-- 

</body>
</html> -->
