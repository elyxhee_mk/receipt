@extends('admin.layouts.master')

<!-- ############ Main START-->

@section('content')
<?php ?>


<div>
	
	<div class="box col-md-11 m-5" >

  <?php $store = $output; if(isset($output) && $output == "" && Session::get('key')){   ?>
		<div class="box-body">
	      <form method="POST" action="{{URL::to('/store/create')}}" enctype="multipart/form-data">
          <input name="_token" type="hidden" value="{{ csrf_token() }}"/>

	        <div class="form-row">
	          <div class="form-group col-md-6">
	            <label for="inputEmail4">Title</label>
	            <input type="text" class="form-control" id="inputEmail4" name="name" placeholder="Title of store" required>
	          </div>
	          <div class="form-group col-md-6">
	            <!-- <label for="inputPassword4">Password</label>
	            <input type="password" class="form-control" id="inputPassword4" placeholder="Password"> -->
	            <label for="inputPassword4">Logo</label><br>
	            <input type="file" class="" id="inputPassword4" name="logo" required> 
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="inputAddress">Address</label>
	          <input type="text" class="form-control" name="address" id="inputAddress" placeholder="1234 Main St" required>
	        </div>
	        <div class="form-group">
	          <label for="inputAddress2">Quote</label>
	          <input type="text" class="form-control" name="quote" id="inputAddress2" placeholder="">
	        </div>
	        <div class="form-row">
	          <div class="form-group col-md-6">
	            <label for="inputCity">Website</label>
	            <input type="website" class="form-control" name="website" id="inputCity" required>
	          </div>
	        </div>
	        <button type="submit" class="btn primary">Submit</button>
	      </form>
	   </div>

  <?php }else{ ?>


  <div class="box-body">
      <div class="scroll-y">

              <button class="btn btn-primary left" data-toggle="modal" data-target="#myModal" >Edit</button>

              <div class="p-4 mt-3 d-flex flex-column align-items-center">
                <img src="{{$output->image_urls['2x']}}" class="w circle animate fadeInUp" alt=".">
                <span class="text-md mt-3 block">{{$output->name}}</span>
                <!-- <small class="text-muted">{{$output->website}}</small> -->
                <!-- <div class="block clearfix mt-3">
                    <a href="#" class="btn btn-icon white btn-social btn-rounded">
                      <i class="fa fa-facebook"></i>
                      <i class="fa fa-facebook indigo"></i>
                    </a>
                    <a href="#" class="btn btn-icon white btn-social btn-rounded">
                      <i class="fa fa-twitter"></i>
                      <i class="fa fa-twitter blue"></i>
                    </a>
                    <a href="#" class="btn btn-icon white btn-social btn-rounded">
                      <i class="fa fa-google-plus"></i>
                      <i class="fa fa-google-plus red"></i>
                    </a>
                    <a href="#" class="btn btn-icon white btn-social btn-rounded">
                      <i class="fa fa-linkedin"></i>
                      <i class="fa fa-linkedin cyan"></i>
                    </a>
                  </div> -->
              </div>
              <div class="p-2 col-4 offset-4">
                    <ul class="nav flex-column">
                        <li class="nav-item text-center">
                            <a class="nav-link d-flex flex-row text-muted">
                              <span class="flex"> <i class="fa fa-fw fa-map-marker"></i> {{$output->address}}</span>
                            </a>
                        </li>
                        <li class="nav-item text-center">
                            <a class="nav-link d-flex flex-row text-muted">
                              <span class="flex"> <i class="fa fa-fw fa-at"></i> {{$output->website}}</span>
                            </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link d-flex flex-row text-muted">
                              <span class="flex">123-456-789</span>
                              <span class="text-sm">
                                <i class="fa fa-fw fa-phone"></i>
                              </span>
                            </a>
                        </li> -->
                       <!--  <li class="nav-item">
                            <a class="nav-link d-flex flex-row text-muted">
                              <span class="flex">Nouvelle@gmail.com</span>
                              <span class="text-sm">
                                <i class="fa fa-fw fa-envelope"></i>
                              </span>
                            </a>
                        </li> -->
                    </ul>
                  </div>

      </div>
  </div>


  <?php } ?>
	</div>

</div>

<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Modal Header</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{URL::to('api/store/update')}}" enctype="multipart/form-data">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
            <input name="store_id" type="hidden" value="{{Session::get('store')}}"/>
            <input name="user_id" type="hidden" value="{{Session::get('key')->id}}"/>

            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputEmail4">Title</label>
                <input type="text" class="form-control" id="inputEmail4" name="name" placeholder="Title" value="{{$output->name}}">
              </div>
              <div class="form-group col-md-6">
                <label for="inputPassword4">Logo</label><br>
                <input type="file" class="" id="inputPassword4" name="file" > 
              </div>
            </div>
            <div class="form-group">
              <label for="inputAddress">Address</label>
              <input type="text" class="form-control" name="address" id="inputAddress" placeholder="1234 Main St" value="{{$output->address}}">
            </div>
            <div class="form-group">
              <label for="inputAddress2">Quote</label>
              <input type="text" class="form-control" name="quote" id="inputAddress2" placeholder="" value="{{$output->quote}}">
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputCity">Website</label>
                <input type="website" class="form-control" name="website" id="inputCity" value="{{$output->website}}">
              </div>
            </div>
            <button type="submit" class="btn primary">Update</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


<script>
    $('#pageTitle').html('Store');
</script>

@stop
<!-- 

</body>
</html> -->
