@extends('admin.layouts.master')

<!-- ############ Main START-->

@section('content')

<div>
	<div>
    <div class="padding">
      <!-- <p>DataTables</p> -->
      <div style="">
      </div>        

      <div class="row mb-2">
        <div class="col-sm-10">
          <h4>Feedbacks</h4>
        </div>
        <div class="col-sm-2" style="text-align: right;" >
          <input type="hidden" name="statuspanel" id="statuspanel" value="">
            <ul class="nav nav-pills" style="text-align: center">
              <li class="nav-item tabbtn btn-primary" id="Unread">
                <a class="nav-link" href="#">Unread</a>
              </li>
              <li class="nav-item tabbtn " id="Read">
                <a class="nav-link" href="#">Read</a>
              </li>
            </ul>
        </div>
      </div>

      <div class="row m-2">
          
      </div>

      <div class="table-responsive">
        <table id="saad" class="table v-middle p-0 m-0 box" data-plugin="dataTable">
          <thead>
            <tr>
              <th>Name</th> 
              <th>Email</th> 
              <th>Phone</th> 
              <th>Message</th>
              <!-- <th>Rating</th> -->
              <th>Date</th>

            </tr>
          </thead>
          <tbody>

           
          </tbody>
        </table>
      </div>
      <br>
      <script type="text/javascript">
        
      </script>

      
      </div>
    </div>



  </div>


  <!-- ############ Main END-->

</div>
<!-- Footer -->
<div class="content-footer white " id="content-footer">
  <div class="d-flex p-3">
   <span class="text-sm text-muted flex">&copy; Copyright. Flatfull</span>
   <div class="text-sm text-muted">Version 1.1.1</div>
 </div>
</div>
</div>
<!-- ############ Content END-->

<!-- ############ LAYOUT END-->
</div>

<script>




  var apiurl = $('base').attr('href');
  function remove(id){
      // console.log(id);
      
      var id=id; 
      var jsonData = {  
        id:id,
      }
      
      var request = $.ajax({
        url: apiurl+'/api/user/remove',
        data: jsonData,
        type: 'DELETE',
        dataType:'json',
      });

      request.done(function(data){
        if(data.response.code == 200){  
          $('.'+id).remove();
        } 
      });
    }



    //serach button

  $ ('.tabbtn').click(function(){
        event.preventDefault();
        window.location = apiurl+"/feedback?status="+$(this).children().text();
        if ($(this).children().text() == 'Read') {
          $('#Unread').removeClass();
          $('#Read').addClass('btn btn-primary');
        }
      });
    

  </script>

  @stop
<!-- 

</body>
</html> -->
