@extends('admin.layouts.master')

<?php 
$data = Session::get('key');
// dd($data);
?>

<!-- ############ Main START-->

@section('content')
<div id="web_content">
	<div class="p-3 light lt box-shadow-0 d-flex">
		<div class="flex">
			<h1 class="text-md mb-1 _400">Welcome back, {{ucwords($data->name)}}.</h1>
			<!-- <small class="text-muted">Last logged in: 03:00 21, July</small> -->
		</div>
	</div>
	<div class="padding">
		<div class="row">
	      <div class="col-sm-6 col-lg-3">
	        <div class="box list-item">
	          <span class="avatar w-40 text-center rounded primary">
	            <span class="fa fa-dollar"></span>
	          </span>
	          <div class="list-body">
	            <h4 class="m-0 text-md">{{$data->followers}} <span class="text-sm">Followers</span></h4>
	            <small class="text-muted"></small>
	          </div>
	        </div>
	      </div>

	      <div class="col-sm-6 col-lg-3">
	        <div class="box list-item">
	          <span class="avatar w-40 text-center rounded info theme">
	            <span class="fa fa-female"></span>
	          </span>
	          <div class="list-body">
	            <h4 class="m-0 text-md">{{($suggestion['suggestion'])}} <span class="text-sm">Suggestions</span></h4>
	            <small class="text-muted">{{($suggestion['seen'])}} Seen.</small>
	          </div>
	        </div>
	      </div>

	      <div class="col-sm-6 col-lg-3">
	        <div class="box list-item">
	          <span class="avatar w-40 text-center rounded success">
	            <span class="fa fa-bookmark"></span>
	          </span>
	          <div class="list-body">
	            <h4 class="m-0 text-md">{{$visits}} <span class="text-sm">Visits</span></h4>
	            <small class="text-muted">this month.</small>
	          </div>
	        </div>
	      </div>

	      <div class="col-sm-6 col-lg-3">
	        <div class="box list-item">
	          <span class="avatar w-40 text-center rounded warning">
	            <span class="fa fa-star"></span>
	          </span>
	          <div class="list-body">
	            <h4 class="m-0 text-md">{{$rating['avg']}} <span class="text-sm">Rating</span></h4>
	            <small class="text-muted">{{$rating['count']}} Users rated.</small>
	          </div>
	        </div>
	      </div>
		</div>


		<div class="row">
			<div class="col-sm-6">
				<div class="box">
					<div class="box-header">
						<h3>Suggestions</h3>

					</div>

					<?php 
					// dd($suggestionlist);
					foreach ($suggestionlist as $key => $value) {
						// dd($value->user->profile_pic == "");
						if ($value->user->profile_pic != "") {
							// dd($value->user);
							$userimg = $value->user->image_urls["1x"];
						}else{
							$userimg = asset('images/default.png');
						}
					?>

					
					<div class="list inset" id="SuggestionList">
						    <div class="list-item " data-id="item-2">
						      <span class="w-40 avatar circle light-blue">
						          <img src="{{$userimg}}" alt=".">
						      </span>
						      <div class="list-body">
						            <a href="" class="item-title _500">{{$value->content}}</a>
						        
						          <div class="item-except text-sm text-muted h-1x">
						            {{$value->email}}
						          </div>
						
						        <div class="item-tag tag hide">
						        </div>
						      </div>
						      <div>
						          <span class="item-date text-xs text-muted">{{$value->category}}</span>
						      </div>
						    </div>
					</div>

					<?php
					}
					?>

				</div>
			</div>

			<div class="col-sm-6">
				<div class="box">
		            <div class="box-header" id="MemberList">
		              <h3>Social Members</h3>
		            </div>
		            <div class="list inset">

				            	<?php 

				            	foreach ($associate_users as $key => $value) {
								// dd($value);

				            	?>
		                  <div class="list-item " data-id="item-4">
				            	
			                    <span class="w-40 avatar circle pink">
			                      <!-- <i class="busy b-white avatar-right"></i> -->
			                        <?php $words = explode(" ", $value->name);
											$acronym = "";

											foreach ($words as $w) {
											  $acronym .= $w[0];
											}
									?>
									{{strtoupper($acronym)}}
			                    </span>
			                    <div class="list-body">
			                          <a href="" class="item-title _500">{{ucwords($value->name)}}</a>
			                      
			              
			                          <div class="item-except text-sm text-muted h-1x">
			                          {{$value->email}}
			                          </div>
			                      <div class="item-tag tag hide">
			                      </div>
			                    </div>
			                    <!-- <div>
			                    	<button class="btn btn-primary">View</button>
			                    </div> -->
		                  </div>
			                    
			                    <?php 
			                    }
			                    ?>



		            </div>
	            </div>
			</div>
		</div>
		
	</div>
</div>


<!-- ############ Main END-->

	    </div>
	    <!-- Footer -->
	    <div class="content-footer white " id="content-footer">
	    	<div class="d-flex p-3">
	    		<span class="text-sm text-muted flex">&copy; Copyright. Flatfull</span>
	    		<div class="text-sm text-muted">Version 1.1.1</div>
	    	</div>
	    </div>
	</div>
	<!-- ############ Content END-->

<!-- ############ LAYOUT END-->
</div>


<script>

	var nonassociate = "{{$error}} "
	if (nonassociate != " ") {
		$('#web_content').empty();
		$('#web_content').append("<div class='alert alert-dismissible alert-danger'>!! "+nonassociate+"</div>");
	}
	// alert($('base').attr('href'));

	// var getAppUrl = function(){
	// 	alert($('base').attr('href'));

	// 	return $('base').attr('href');
	// };




	// SuggestionListing();
    $('#pageTitle').html('Dashboard');





    function SuggestionListing(){
    	var dataApiUrl = $('base').attr('href')
    	var request = $.ajax({
			url: dataApiUrl,
			data: jsonData,
			type: 'GET',
			dataType:'json',
			headers: {"Authorization":"Bearer "+dataTokenGet},
		});


		request.done(function(data){

			if(data.response.code == 200) {	
				console.log(data.response);
			}
		});
    }



</script>
@stop
<!-- 

</body>
</html> -->
