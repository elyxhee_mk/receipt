<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
  <script src="{{asset('js/libs/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap -->
  <script src="{{asset('js/libs/popper.js/dist/umd/popper.min.js')}}"></script>
  <script src="{{asset('js/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- core -->
  <script src="{{asset('js/libs/pace-progress/pace.min.js')}}"></script>
  <script src="{{asset('js/libs/pjax/pjax.js')}}"></script>

  <script src="{{asset('js/scripts/lazyload.config.js')}}"></script>
  <script src="{{asset('js/scripts/lazyload.js')}}"></script>
  <script src="{{asset('js/scripts/plugin.js')}}"></script>
  <script src="{{asset('js/scripts/nav.js')}}"></script>
  <script src="{{asset('js/scripts/scrollto.js')}}"></script>
  <script src="{{asset('js/scripts/toggleclass.js')}}"></script>
  <script src="{{asset('js/scripts/theme.js')}}"></script>
  <script src="{{asset('js/scripts/ajax.js')}}"></script>
  <script src="{{asset('js/scripts/app.js')}}"></script>
<!-- endbuild -->