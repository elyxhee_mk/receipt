<div class="app" id="app">

<!-- ############ LAYOUT START-->

	<!-- ############ Aside START-->
	<div id="aside" class="app-aside fade  nav-expand white" aria-hidden="true" style="color: white;">
		<div class="sidenav modal-dialog dk" style="color: white;">
		  <!-- sidenav top -->
		  <div class="navbar" style="color: white;">
		    <!-- brand -->
		    <a href="{{URL::to('/users')}}" class="navbar-brand">

		    	<img src="{{asset('images/logo.png')}}">	
		    	<span class="hidden-folded d-inline">Reciept App</span>
		    </a>
		    <!-- / brand -->
		  </div>
		
		  <!-- Flex nav content -->
		  <div class="flex hide-scroll">
		      <div class="scroll">
		        <div class="nav-active-theme" data-nav>
		          <ul class="nav bg">
		            
		              <!-- <li>
		                <a href="{{URL::to('admin/dashboard')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-dashboard"></i>
		                  </span>
		                  <span class="nav-text">Dashboard</span>
		                </a>
		              </li> -->
		              <li>
		                <a href="{{URL::to('/users')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-user"></i>
		                  </span>
		                  <span class="nav-text">User</span>
		                </a>
		              </li>
		              <!-- <li>
		                <a href="{{URL::to('/folders')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-folder"></i>
		                  </span>
		                  <span class="nav-text">Folders</span>
		                </a>
		              </li> -->
		              <li>
		                <a href="{{URL::to('/feedback')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-comment"></i>
		                  </span>
		                  <span class="nav-text">Feedbacks</span>
		                </a>
		              </li>
		              <li>
		                <a href="{{URL::to('/settings')}}">
		                  <span class="nav-icon">
		                    <i class="fa fa-gears"></i>
		                  </span>
		                  <span class="nav-text">Settings</span>
		                </a>
		              </li>
		              
		              


		              
		              <li class="pb-2 hidden-folded"></li>
		          </ul>
		          
		        </div>
		      </div>
		  </div>
		  
		  <!-- sidenav bottom -->
		  <div class="no-shrink lt">
		    <div class="nav-fold">
		    	
		    	<div class="hidden-folded flex p-2-3 bg">
		    		<div class="d-flex p-1">
		    			<span href="app.inbox.html" class="flex text-nowrap">
		    			</span>
		    			<a href="{{URL::to('/logout')}}" class="px-2" data-toggle="tooltip" title="Logout">
		    				<i class="fa fa-power-off text-muted"></i>
		    			</a>
		    		</div>
		    	</div>
		    </div>
		  </div>
		</div>
	</div>
	<!-- ############ Aside END-->

	<!-- ############ Content START-->
	<div id="content" class="app-content box-shadow-1 box-radius-1" role="main">
		<!-- Header -->
	    <div class="content-header white  box-shadow-1" id="content-header">
	    	<div class="navbar navbar-expand-lg">
	    	  
	    	  <!-- Page title -->
	    	  <div class="navbar-text nav-title flex" id="pageTitle"></div>
	    	
	    	  <ul class="nav flex-row order-lg-2">
	    	    <!-- Notification -->
	    	    
	    	
	    	    <!-- User dropdown menu -->
	    	    <li class="dropdown d-flex align-items-center">
	    	        <a href="{{URL::to('/logout')}}"><i class="fa fa-power-off"></i></a>
	    	    </li>
	    	    <!-- Navarbar toggle btn -->
	    	    <li class="d-lg-none d-flex align-items-center">
	    	      <a href="#" class="mx-2" data-toggle="collapse" data-target="#navbarToggler">
	    	      </a>
	    	    </li>
	    	  </ul>
	    	  <!-- Navbar collapse -->
	    		
	    	
	    	</div>
	    </div>
	    <!-- Main -->
		<div class="content-main " id="content-main">
