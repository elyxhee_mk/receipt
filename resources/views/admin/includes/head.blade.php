<meta charset="utf-8" />
  <title>Reciept App</title>
  <meta name="description" content="Reciept App" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <base href="{{URL::to('/')}}">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="{{asset('images/favicon.png')}}">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="{{asset('images/favicon.png')}}">
  
  <!-- style -->

  <link rel="stylesheet" href="{{asset('js/libs/font-awesome/css/font-awesome.min.css')}}" type="text/css" />

  <!-- build:css ../assets/css/app.min.css -->
  <link rel="stylesheet" href="{{asset('js/libs/bootstrap/dist/css/bootstrap.min.css')}}" type="text/css" />
  <link rel="stylesheet" href="{{asset('css/assets/css/app.css')}}" type="text/css" />
  <link rel="stylesheet" href="{{asset('css/assets/css/style.css')}}" type="text/css" />

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


  <!-- Moved these files to other JS file -->

  <script type="text/javascript">

  $(document).ready(function(){

  $('.pagination .active').css({"padding": ".5rem .75rem",
          "border": "1px solid rgba(120, 130, 140, 0.13)","color":"white","background-color": "#2e93f9"})
  $('.disabled').css({"padding": ".5rem .75rem",
          "border": "1px solid rgba(120, 130, 140, 0.13)"})

  });
   
  </script>