@include('admin.includes.head')




<?php 
$status='';
if (Session::has('status')) {
  $status = Session::get('status');
?>

  <script>
    $(document).ready(function(){
      $('#alert-box').removeClass('hide');
    });
  </script>

<?php
}

?>

<div class="d-flex flex-column flex">
  <div class="navbar light bg pos-rlt box-shadow">
    <div class="mx-auto">
      <!-- brand -->
      
      <!-- / brand -->
    </div>
  </div>
  <div id="content-body">
    <div class="py-5 text-center w-100">
      <div class="mx-auto w-xxl w-auto-xs">
        <div class="px-3">
          <div>
            <img src="{{asset('images/logo.png')}}" height="100">
          </div>
          <br><br>
         
          <form name="form" action="{{URL::to('/actlogin')}}" method="POST">
          <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
          <!-- <input name="login_via" type="hidden" value="web"/> -->

            <div class="form-group">
              <input type="email" class="form-control" placeholder="Email" name="email" value="<?php //echo $email; ?>" required>
            </div>
            <div class="form-group">
              <input type="password" class="form-control" placeholder="password" name="password" required>
            </div>
            <div class="alert alert-dismissible alert-danger hide" id="alert-box">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <strong class="alert" >{{$status}}</strong>.

            </div>


            <div class="mb-3">        
              <label class="md-check">
                <input type="checkbox"><i class="primary"></i> Keep me signed in
              </label>
            </div>
            <button type="submit" class="btn primary">Sign in</button>
          </form>
         
        </div>
      </div>
    </div>
  </div>
</div>



@include('admin.includes.script')
</body>
</html>

<script type="text/javascript">
  $(document).ready(function(){

   });
 </script>