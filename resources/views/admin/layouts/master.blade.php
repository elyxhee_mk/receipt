<!doctype html>
    <html lang="en">
        @include('admin.includes.head')
    
   
    
        @include('admin.includes.leftpanel')
             @yield('content')
        @include('admin.includes.script')
            <!-- @yield('after-footer') -->

        @yield('scripts')
    </body>
</html>