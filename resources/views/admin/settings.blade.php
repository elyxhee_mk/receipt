@extends('admin.layouts.master')

<!-- ############ Main START-->

@section('content')


      <div class="padding">
        <div class="row">
          <div class="col-sm-6 offset-sm-3 box pt-3">
            <h3>Settings</h3>
            <hr>
            <form method="post" action="{{URL::to('/update')}}">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="form-group">
                <label>Old Password</label>
                <input type="password" name="old_password" id="oldpass" class="form-control" required>
              </div>
              <div class="form-group">
                <label>New password</label>
                <input type="password" name="password" id="newpass" class="form-control" required>
              </div>
              <div class="form-group">
                <label>Confirm password</label>
                <input type="password" name="confirm" id="confirmpass" class="form-control" required>
                <div id="error"></div>
              </div>
              <div class="form-group">
                <input type="Submit" name="oldpassword" id="submit" class="btn btn-primary">
              </div>

               <?php if (Session::has('update')){ ?>
                    <div class="alert alert-success">
                      {{Session::get('update')}}
                    </div>
                <?php }elseif(Session::has('error')){ ?>
                  <div class="alert alert-danger">
                      {{Session::get('error')}}
                  </div>
                <?php }?>

            </form>
            
          </div>
         

        </div>

      </div>




      <!-- Footer -->
     <!--  <div class="content-footer white " id="content-footer">
        <div class="d-flex p-3">
          <span class="text-sm text-muted flex">&copy; Copyright. Flatfull</span>
          <div class="text-sm text-muted">Version 1.1.1</div>
        </div>
      </div> -->
  </div>
  <!-- ############ Content END-->

<!-- ############ LAYOUT END-->
</div>

<script>

$("#confirmpass").keyup(function(){
  if($('#newpass').val()===$(this).val()){
    $('#error').text('');
    $("input[type=submit]").removeAttr("disabled");
  }else{
    $('#error').text('*password not match');
    $('#error').css('color','red');
    $("input[type=submit]").attr("disabled", "disabled");
  }
});

</script>

@stop
<!-- 

</body>
</html> -->
