<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('auth/register', 'UserController@register');
Route::post('/', 'UserController@register');
Route::post('auth/login', 'UserController@login');
Route::post('auth/logout', 'UserController@logout');


//----------USER MODULE----------------
Route::post('/register', ['uses'=>'UserController@register']);
Route::post('/login', ['uses'=>'UserController@login']);
Route::put('/forgot-password',['as'=>'user.forgot-password','uses'=> 'UserController@forgotPassword']);
Route::put('/reset-password',['as'=>'user.reset-password','uses' => 'UserController@resetPassword']);
Route::post('/user/profile/view', ['uses'=>'UserController@view']);
Route::post('/user/profile/upload', ['uses'=>'UserController@upload']);

Route::post('/user/visibility', ['uses'=>'UserController@setVisibility']);
Route::post('/user/profile/update', ['uses'=>'UserController@update']);
Route::get('/user/token/refresh', ['uses'=>'UserController@refreshToken']);
Route::post('/device-token/set', ['uses'=>'DeviceController@setUserTokens']);
Route::post('/device-token/remove', ['uses'=>'DeviceController@removeUserTokens']);
Route::get('/user/check', ['uses'=>'UserController@check']);



//User Search
Route::get('/user/search',['as'=>'user.search','uses'=> 'UserController@userSearch']);
//--------------------------------------
/*Route::get('auth/google', 'UserController@redirectToGoogle');
Route::get('auth/google/callback', 'UserController@handleGoogleCallback');*/

Route::group(['middleware' => array('jwt.auth','validaterole')], function () {
	Route::get('/user/list', ['uses'=>'UserController@all']);
    Route::post('/order/update', ['uses'=>'OrderController@update']);
    Route::post('/content/add', ['uses'=>'ContentController@add']);
	Route::post('/content/delete', ['uses'=>'ContentController@delete']);//delete
    
    //Route::post('/content/add', ['uses'=>'OrderController@add']);
    
});
Route::group(['middleware' => 'jwt.auth'], function () {
	
    Route::post('/folder/create', ['uses'=>'FolderController@add']); 
    Route::post('/folder/rename', ['uses'=>'FolderController@rename']);
    Route::post('/folder/delete', ['uses'=>'FolderController@delete']);
    Route::get('/folder/all', ['uses'=>'FolderController@all']);
    Route::post('/receipt/create', ['uses'=>'ReceiptController@add']); 
    Route::post('/receipt/update', ['uses'=>'ReceiptController@update']);
    Route::get('/receipt/list', 'ReceiptController@all');
    Route::post('/receipt/delete', ['uses'=>'ReceiptController@delete']);
    Route::post('/user/feedback', ['uses'=>'UserController@feedback']);

    Route::post('/order/add', ['uses'=>'OrderController@add']);
    Route::get('/order/all', ['uses'=>'OrderController@all']);
    
    Route::get('/content/get', ['uses'=>'ContentController@all']);
Route::post('/change-password', ['uses'=>'UserController@changePassword']);
	
	

    
    
});

// activity
Route::get('/activity/notification/list', ['as'=>'activity.notification.list','uses'=>'ActivityController@activityNotificationList']);
Route::post('/activity/notification/view', ['as'=>'activity.notification.view','uses'=>'ActivityController@activityNotificationView']);
Route::get('/activity/count/list', ['as'=>'activity.count.list','uses'=>'ActivityController@activityCountList']);


