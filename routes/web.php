<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.login');
});
Route::post('/actlogin','HomeController@login');


Route::group(['middleware'=>'admin'],function(){


	Route::get('/users', 'HomeController@showUsers');
	Route::get('/folders', 'HomeController@showFolders');
});



Route::get('/active', 'HomeController@ChangeActive');

Route::get('/feedback', function () {
    return view('admin.feedback');
});
Route::get('/settings', function () {
    return view('admin.settings');
});
Route::post('/update', 'HomeController@update');

Route::get('/logout', function () {
	Session::flush();
    return redirect('/');
});
